// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
      apiKey: "AIzaSyDK5iSAln72P8VYOxcW_gG1baoiHt9HS5w",
      authDomain: "prortype-7bc91.firebaseapp.com",
      databaseURL: "https://prortype-7bc91.firebaseio.com",
      projectId: "prortype-7bc91",
      storageBucket: "prortype-7bc91.appspot.com",
      messagingSenderId: "276169523385",
      appId: "1:276169523385:web:07f2b4feeafd3c87399b68",
      measurementId: "G-B7NQ6G6TDB"
}
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
