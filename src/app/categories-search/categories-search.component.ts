import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-categories-search',
  templateUrl: './categories-search.component.html',
  styleUrls: ['./categories-search.component.scss']
})
export class CategoriesSearchComponent implements OnInit {
  screenWidth: number;

  rowsNumber: number;
  columnNumber: number;

  promoFields = [
    {   "Id": 1,
        "Name":"Fighting"
    },
    { "Id": 2,
      "Name":"Girl"
    },
    { "Id": 3,
      "Name":"BasketBall"
    },
    { "Id": 4,
    "Name":"Football"
    },
    { "Id": 5,
    "Name":"Puzzle"
    }
    ,
    { "Id": 6,
    "Name":"Crazy"
    }
    ,
    { "Id": 7,
    "Name":"Fighting"
    }
    ,
    { "Id": 8,
    "Name":"Racing"
    }
    ,
    { "Id": 9,
    "Name":"Idle"
    }
    ,
    { "Id": 10,
    "Name":"war"
    }
    ,
    { "Id": 11,
    "Name":"racing"
    }

    ];

  constructor( ) { }

  ngOnInit(): void {
    this.getScreenSize();
  }

  getDetails(number){
    var gameName = this.promoFields.find(x=>x.Id == number);
    return gameName;
  }

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
        this.screenWidth = window.innerWidth; 
        let screen = this.screenWidth;

        if (screen > 1400) 
        {
          this.rowsNumber = 3;
          this.columnNumber = 10;
        }  else if (screen < 1401 && screen > 1160)
        {
          this.rowsNumber = 4;
          this.columnNumber = 8;
        } else if (screen < 1161 )
        {
          this.rowsNumber = 5;
          this.columnNumber = 6;
        }
  }

}
