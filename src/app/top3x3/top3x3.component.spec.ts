import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Top3x3Component } from './top3x3.component';

describe('Top3x3Component', () => {
  let component: Top3x3Component;
  let fixture: ComponentFixture<Top3x3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Top3x3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Top3x3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
