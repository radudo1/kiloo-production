import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Renderer2,
  HostListener,
  AfterViewInit,
  AfterContentInit,
} from "@angular/core";
import { Language } from "../models/language";
import { NavbarService } from "../services/navbar.service";
import { Router, NavigationStart } from "@angular/router";
import { DataService } from "../services/data.service";
import { Animations } from "../animations/slide-in-out/slide-in-out.component";
import { ClickOutsideDirective } from "../click-outside.directive";
import { timeout } from "rxjs/operators";

@Component({
  selector: "app-search-bar",
  templateUrl: "./search-bar.component.html",
  styleUrls: ["./search-bar.component.scss"],
  animations: Animations,
})
export class SearchBarComponent implements OnInit {
  @ViewChild('menuIcon', { read: ElementRef, static: false })  menuIcon: ElementRef;
  @ViewChild('menuPopup', { read: ElementRef, static: false })  menuPopup: ElementRef;
  //Variables
  selectedLanguage: Language;
  searchInput: String = "";
  searchResult: Array<any> = [];
  searchedCategory: string;
  searchBarOpen: string;
  filled: boolean;
  screenWidth: number;
  barWidth: number;
  clicked: boolean;
  languageClicked: boolean;
  focused: boolean;
  languageBarOpen: string;

  searchUrl: string = "/search";

  //Data
  public gameList: Array<any> = [
    {
      name: "Subway Surfers",
      link: "http://localhost:4200/game/23",
      description:
        "Structural Engineer Michael Scofield turns himself into the Fox River Penitentiary in order to break out his brother Lincoln Burrows, who is on death row for the murder of the Vice President's brother. But Lincoln was set up by some of the Company (an agency formed by corrupt government officials) guys, headed by General Jonathan Krantz. Michael breaks out from Fox River with his brother Lincoln and other convicts.",
      category: "Running Games (54)",
      releaseDate: "29 August 2005 (USA)",
    },
    {
      name: "Bullet League Robogeddon",
      link: "http://localhost:4200/game/23",
      description:
        "The adventures of Ragnar Lothbrok: the greatest hero of his age. The series tells the saga of Ragnar's band of Viking brothers and his family as he rises to become King of the Viking tribes. As well as being a fearless warrior, Ragnar embodies the Norse traditions of devotion to the gods: legend has it that he was a direct descendant of Odin, the god of war and warriors.",
      category: "Shooting Games (32)",
      releaseDate: "3 March 2013 (USA)",
    },
    {
      name: "Person of Interest",
      link: "http://localhost:4200/game/23",
      description:
        "A billionaire software-genius named Harold Finch creates a Machine for the government that is designed to detect acts of terror before they can happen, by monitoring the entire world through every cell-phone, email and surveillance camera. Finch discovered that the machine sees everything, potential terrorist acts and violent crimes that involve ordinary people.",
      category: "Running Games (54)",
      releaseDate: "22 September 2011 (USA)",
    },
  ];

  constructor(
    public nav: NavbarService,
    private renderer: Renderer2,
    private router: Router,
    private sharingService: DataService,
    private elRef:ElementRef
    
  ) {

    router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
      }
      // NavigationEnd
      // NavigationCancel
      // NavigationError
      // RoutesRecognized

      this.searchResult = [];
    });
    this.renderer.listen('window', 'click', (e: Event) => {
      if (
       (this.menuPopup && this.menuPopup.nativeElement.contains(e.target)) ||
        (this.menuIcon && this.menuIcon.nativeElement.contains(e.target))
       ) {
            // Clicked inside plus preventing click on icon
           if(this.selectedLanguage.name != this.selectedLanguage.name ){
            this.languageBarOpen = "hidden"
           }
         } else {
           // Clicked outside
           this.languageBarOpen = "hidden"
       }
    });
  }
  ngOnInit(): void {
    this.searchBarOpen = "out";
    this.languageBarOpen = "hidden";
    this.selectedLanguage = this.languages[4];
    this.getScreenSize();
  }


  fetchGames(event: any) {
    if (event.target.value === "") {
      this.searchedCategory === "";
      return (this.searchResult = []);
    }
    this.searchResult = this.gameList.filter((series) => {
      this.searchedCategory = series.category;
      return series.name
        .toLowerCase()
        .startsWith(event.target.value.toLowerCase());
    });
    if (this.searchResult.length === 0) {
      console.log("Sorry no games");
    }
  }

  toggleHelpMenu(): void {
    this.clicked = true;
    if (this.clicked && this.focused) {
      this.searchBarOpen = "in";
    } else if (this.clicked && this.searchInput.length != 0) {
      this.searchBarOpen = "out";
    } else if (this.searchInput.length == 0 && this.focused) {
      this.searchBarOpen = "out";
    }
  }
  toggleLanguageMenu(): void {
    this.languageBarOpen = this.languageBarOpen === 'hidden' ? 'visible' : 'hidden';
  }
  changeComponent(url: string) {
    this.sharingService.setData(this.searchResult[0].name);
    this.router.navigateByUrl(url);
  }
  searchWithEnter(url: string) {
    if (this.searchInput.length != 0) {
      this.sharingService.setData(this.searchResult[0].name);
      this.router.navigateByUrl(url); //redirects url to new component
    }
  }

  onFocus() {
    this.focused = true;
    this.languageBarOpen = "hidden";
  }
  onBlur() {
    this.focused = false;
    if (this.searchInput.length == 0) {
      this.searchResult = [];
    }
    if (this.searchInput.length == 0 && !this.focused) {
      this.searchBarOpen = "out";
    }
  }


  @HostListener("window:resize", ["$event"])
  getScreenSize(event?) {
    this.screenWidth = window.innerWidth;
    let screen = this.screenWidth;
    if (screen > 1500) {
      this.barWidth = 1020;
    } else if (screen < 1501 && screen > 1400) {
      this.barWidth = 900;
    } else if (screen < 1401 && screen > 1280) {
      this.barWidth = 800;
    } else if (screen < 1281 && screen > 1160) {
      this.barWidth = 710;
    } else if (screen < 1161 && screen > 1040) {
      this.barWidth = 610;
    } else if (screen < 1401 && screen > 920) {
      this.barWidth = 490;
    } else if (screen < 920) {
      this.barWidth = 380;
    }
  }

  languages = [
    new Language(1, "Portugese", "../../assets/img/flags/pt.png"),
    new Language(2, "French", "../../assets/img/flags/fr.png"),
    new Language(3, "Spanish", "../../assets/img/flags/es.png"),
    new Language(4, "English", "../../assets/img/flags/gb.png"),
    new Language(16, "German", "../../assets/img/flags/de.png"),
    new Language(5, "Arabic", "../../assets/img/flags/ar.png"),
    new Language(6, "Italian", "../../assets/img/flags/it.png"),
    new Language(7, "Polish", "../../assets/img/flags/pl.png"),
    new Language(8, "Turkish", "../../assets/img/flags/tr.png"),
    new Language(9, "Russian", "../../assets/img/flags/ru.png"),
    new Language(10, "Brazilian Portugese", "../../assets/img/flags/ptr.png"),
    new Language(11, "Bahasa Indonesian", "../../assets/img/flags/id.png"),
    new Language(12, "Swedish", "../../assets/img/flags/se.png"),
    new Language(13, "Norwegian", "../../assets/img/flags/no.png"),
    new Language(14, "Hindi", "../../assets/img/flags/in.png"),
    new Language(15, "Dutch", "../../assets/img/flags/nl.png"),
  ];

  onSelect(language: Language): void {
    if(this.languageBarOpen === "visible"){
      this.selectedLanguage = language;
      this.languageBarOpen = "hidden";
    }
 
  }
}
