import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavGameComponent } from './nav-game.component';

describe('NavGameComponent', () => {
  let component: NavGameComponent;
  let fixture: ComponentFixture<NavGameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavGameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
