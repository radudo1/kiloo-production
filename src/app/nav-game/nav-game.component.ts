import { trigger } from '@angular/animations';
import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { NavGameService } from '../services/nav-game.service';
import { Language } from '../models/language';

@Component({
  selector: 'app-nav-game',
  templateUrl: './nav-game.component.html',
  styleUrls: ['./nav-game.component.scss']
})
export class NavGameComponent implements OnInit {
  
  @ViewChild('trigger') trigger;
  @ViewChild('menu') menu;
  selectedLanguage: Language;
  displayLanguageList: boolean;
  public searchInput: String = '';



  constructor(public navGame: NavGameService,
      private renderer: Renderer2
    ) { 
    }



  ngOnInit(): void {
    this.displayLanguageList = false;
    this.selectedLanguage = this.languages[4];
  }
  
  show(){
    this.displayLanguageList = true;
  }
    
  languages = [
    new Language(1, 'Portugese', '../../assets/img/flags/pt.png'),
    new Language(2, 'French', '../../assets/img/flags/fr.png'),
    new Language(3, 'Spanish', '../../assets/img/flags/es.png'),
    new Language(4, 'English', '../../assets/img/flags/gb.png'),
    new Language(16, 'German', '../../assets/img/flags/de.png'),
    new Language(5, 'Arabic', '../../assets/img/flags/ar.png'),
    new Language(6, 'Italian', '../../assets/img/flags/it.png'),
    new Language(7, 'Polish', '../../assets/img/flags/pl.png'),
    new Language(8, 'Turkish', '../../assets/img/flags/tr.png'),
    new Language(9, 'Russian', '../../assets/img/flags/ru.png'),
    new Language(10, 'Brazilian Portugese', '../../assets/img/flags/ptr.png'),
    new Language(11, 'Bahasa Indonesian', '../../assets/img/flags/id.png'),
    new Language(12, 'Swedish', '../../assets/img/flags/se.png'),
    new Language(13, 'Norwegian', '../../assets/img/flags/no.png'),
    new Language(14, 'Hindi', '../../assets/img/flags/in.png'),
    new Language(15, 'Dutch', '../../assets/img/flags/nl.png'),
  ];

  onSelect(language: Language): void {
    this.selectedLanguage = language;
  }


}
