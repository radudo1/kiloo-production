import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Top6xComponent } from './top6x.component';

describe('Top6xComponent', () => {
  let component: Top6xComponent;
  let fixture: ComponentFixture<Top6xComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Top6xComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Top6xComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
