import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Top7xComponent } from './top7x.component';

describe('Top7xComponent', () => {
  let component: Top7xComponent;
  let fixture: ComponentFixture<Top7xComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Top7xComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Top7xComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
