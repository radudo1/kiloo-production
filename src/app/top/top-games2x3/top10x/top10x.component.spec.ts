import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Top10xComponent } from './top10x.component';

describe('Top10xComponent', () => {
  let component: Top10xComponent;
  let fixture: ComponentFixture<Top10xComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Top10xComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Top10xComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
