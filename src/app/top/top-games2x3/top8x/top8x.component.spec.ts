import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Top8xComponent } from './top8x.component';

describe('Top8xComponent', () => {
  let component: Top8xComponent;
  let fixture: ComponentFixture<Top8xComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Top8xComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Top8xComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
