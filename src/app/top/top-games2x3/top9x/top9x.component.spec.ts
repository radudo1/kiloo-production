import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Top9xComponent } from './top9x.component';

describe('Top9xComponent', () => {
  let component: Top9xComponent;
  let fixture: ComponentFixture<Top9xComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Top9xComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Top9xComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
