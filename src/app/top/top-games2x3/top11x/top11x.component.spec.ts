import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Top11xComponent } from './top11x.component';

describe('Top11xComponent', () => {
  let component: Top11xComponent;
  let fixture: ComponentFixture<Top11xComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Top11xComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Top11xComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
