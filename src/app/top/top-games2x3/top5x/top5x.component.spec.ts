import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Top5xComponent } from './top5x.component';

describe('Top5xComponent', () => {
  let component: Top5xComponent;
  let fixture: ComponentFixture<Top5xComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Top5xComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Top5xComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
