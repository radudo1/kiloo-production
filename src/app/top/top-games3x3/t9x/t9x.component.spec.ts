import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { T9xComponent } from './t9x.component';

describe('T9xComponent', () => {
  let component: T9xComponent;
  let fixture: ComponentFixture<T9xComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ T9xComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(T9xComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
