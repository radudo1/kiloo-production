import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { T11xComponent } from './t11x.component';

describe('T11xComponent', () => {
  let component: T11xComponent;
  let fixture: ComponentFixture<T11xComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ T11xComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(T11xComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
