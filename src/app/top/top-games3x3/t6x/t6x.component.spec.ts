import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { T6xComponent } from './t6x.component';

describe('T6xComponent', () => {
  let component: T6xComponent;
  let fixture: ComponentFixture<T6xComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ T6xComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(T6xComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
