import { Component, OnInit, Renderer2, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-t6x',
  templateUrl: './t6x.component.html',
  styleUrls: ['./t6x.component.scss']
})
export class T6xComponent implements OnInit {
  video1On: boolean = false;
  video2On: boolean = false;
  video3On: boolean = false;
  constructor(
    private renderer: Renderer2
  ) { }

  ngOnInit(): void {
  }
  numSequence(n: number): Array<number> { 
    return Array(n); 
  } 

  @ViewChild('videoPlayerx10') videoplayer: ElementRef;
  @ViewChild('videoPlayer2') videoplayer2: ElementRef;
  @ViewChild('videoPlayer3') videoplayer3: ElementRef;
  @ViewChild('videoPlayerEditor') videoPlayerEditor: ElementRef;
  
  toggleVideo(event: any) {
    this.video1On = true;
    let  videoPlayer = this.videoplayer.nativeElement;
    this.renderer.setStyle(videoPlayer, 'opacity', '1' );
    this.renderer.setStyle(videoPlayer,  'transition', '0.3s  ease-in-out 0.3s ');
    videoPlayer.play();

  }
  toggleVideo2(event: any) {
    this.video2On = true;
    let  videoPlayer = this.videoplayer2.nativeElement;
     this.renderer.setStyle(videoPlayer, 'opacity', '1' );
     this.renderer.setStyle(videoPlayer,  'transition', '0.3s  ease-in-out 0.3s ');
     videoPlayer.play();
  }
  toggleVideo3(event: any) {
    this.video3On = true;
    let  videoPlayer = this.videoplayer3.nativeElement;
     this.renderer.setStyle(videoPlayer, 'opacity', '1' );
     this.renderer.setStyle(videoPlayer,  'transition', '0.3s  ease-in-out 0.3s ');
     videoPlayer.play();

  }
  toggleVideoEditor(event: any) {
    let  videoPlayer = this.videoPlayerEditor.nativeElement;
     this.renderer.setStyle(videoPlayer, 'opacity', '1' );
     this.renderer.setStyle(videoPlayer, 'z-index', '9998' );
     this.renderer.setStyle(videoPlayer,  'transition', '0.3s  ease-in-out 0.3s ');
     videoPlayer.play();
  }
  hideVideo(event:any){
    let  videoPlayer = this.videoplayer.nativeElement;
    let videoPlayer2 = this.videoplayer2.nativeElement;
    let videoPlayer3 = this.videoplayer3.nativeElement;
    let videoPlayerEditor = this.videoPlayerEditor.nativeElement;
    this.renderer.setStyle(videoPlayer, 'opacity', '0' );
    this.renderer.setStyle(videoPlayer2, 'opacity', '0');
    this.renderer.setStyle(videoPlayer3, 'opacity', '0');
    this.renderer.setStyle(videoPlayerEditor, 'opacity', '0');
    this.video1On = false;
    this.video2On = false;
    this.video3On = false;
  }

}