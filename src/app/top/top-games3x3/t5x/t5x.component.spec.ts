import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { T5xComponent } from './t5x.component';

describe('T5xComponent', () => {
  let component: T5xComponent;
  let fixture: ComponentFixture<T5xComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ T5xComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(T5xComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
