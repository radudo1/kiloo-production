import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { T8xComponent } from './t8x.component';

describe('T8xComponent', () => {
  let component: T8xComponent;
  let fixture: ComponentFixture<T8xComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ T8xComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(T8xComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
