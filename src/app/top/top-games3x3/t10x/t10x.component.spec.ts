import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { T10xComponent } from './t10x.component';

describe('T10xComponent', () => {
  let component: T10xComponent;
  let fixture: ComponentFixture<T10xComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ T10xComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(T10xComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
