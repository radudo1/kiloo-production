import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { T7xComponent } from './t7x.component';

describe('T7xComponent', () => {
  let component: T7xComponent;
  let fixture: ComponentFixture<T7xComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ T7xComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(T7xComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
