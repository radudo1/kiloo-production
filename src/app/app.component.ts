import { Component, HostListener, OnInit, Renderer2 } from '@angular/core';
import { environment } from '../environments/environment'
import { Subscription } from 'rxjs';
import { NgcCookieConsentService, NgcInitializeEvent, NgcStatusChangeEvent, NgcNoCookieLawEvent, NgcContentOptions } from 'ngx-cookieconsent';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],

})

export class AppComponent implements OnInit {
  title = 'Prototype Version 1';
  adBlockOn: boolean = false;
  cookieAccepted: string = 'false';


  constructor(
    private cookieService: CookieService,
    ) { }
  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.

    this.cookieAccepted = this.cookieService.get('accepted');
    console.log(this.cookieService.get('accepted'));
  }
  detected(isDetected) {
    this.adBlockOn = isDetected;
    console.log(`Adblock Detected: ${isDetected}`);
    if(this.adBlockOn === true){
      this.stopScroll();
    }else{
      this.resumeScroll();
    }

  }
  noScroll() {
    window.scrollTo(0, 0);
  }
  stopScroll(){
  // add listener to disable scroll
  window.addEventListener('scroll', this.noScroll);
  }
  resumeScroll(){
    // Remove listener to re-enable scroll
    window.removeEventListener('scroll', this.noScroll);
  }
  changeHide(val: boolean) {
    this.adBlockOn = val;
    this.resumeScroll();
  }
  

}


