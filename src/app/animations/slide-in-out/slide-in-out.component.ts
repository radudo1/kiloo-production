import { style, trigger, state, transition, animate } from "@angular/animations";

export const Animations = [
  trigger('slideInOut', [
    state('in', style({
      height: '*',
      width: `{{barWidth}}px`,
      zIndex: '9999999'
    }),  {params: {barWidth: 1020}}),
    state('out', style({
   
    })),
    transition('in => out', animate('400ms ease-in-out')),
    transition('out => in', animate('400ms ease-in-out'))
  ]),
  trigger('showMe', [
    state('visible', style({
      opacity: `1`,
      height: `*`
    })),
    state('hidden', style({
      opacity: `0`,
      height: `0`,
      display: 'none'
    })),
    transition('visible => hidden', animate('200ms ease-in-out')),
    transition('hidden => visible', animate('200ms ease-in-out'))
  ])
];