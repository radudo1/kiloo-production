import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SlideInOutComponent } from './slide-in-out.component';

describe('SlideInOutComponent', () => {
  let component: SlideInOutComponent;
  let fixture: ComponentFixture<SlideInOutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SlideInOutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SlideInOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
