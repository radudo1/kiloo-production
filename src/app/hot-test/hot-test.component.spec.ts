import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotTestComponent } from './hot-test.component';

describe('HotTestComponent', () => {
  let component: HotTestComponent;
  let fixture: ComponentFixture<HotTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
