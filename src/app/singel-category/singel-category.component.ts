import { Component, OnInit, HostListener } from '@angular/core';
import { NavbarService } from '../services/navbar.service';
import { FooterService } from '../services/footer.service';
import { ActivatedRoute } from '@angular/router';
import { NavGameComponent } from '../nav-game/nav-game.component';
import { NavGameService } from '../services/nav-game.service';
import { RatingService } from '../services/rating.service';


@Component({
  selector: 'app-singel-category',
  templateUrl: './singel-category.component.html',
  styleUrls: ['./singel-category.component.scss']
})
export class SingelCategoryComponent implements OnInit {
  screenWidth: number;
  rowsNumber: number;
  categoryName: string;
  columnNumber: number;

  constructor(private nav:  NavbarService,
              private footer: FooterService,
              private activatedRoute: ActivatedRoute,
              private navG: NavGameService,
              private rating: RatingService
            ) { }

  ngOnInit(): void {
    this.nav.show();
    this.footer.show();
    this.navG.hide();
    this.rating.show();
    window.scrollTo(0,0);
    this.getScreenSize();
    this.categoryName = this.activatedRoute.snapshot.paramMap.get('name');
  }

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
        this.screenWidth = window.innerWidth; 
        let screen = this.screenWidth;

        if ( screen > 1500) 
        {
          this.rowsNumber = 6;
          this.columnNumber = 11;
        } 
        else if (screen < 1501 && screen > 1400)
        {
          this.rowsNumber = 7;
          this.columnNumber = 10;
        }
        else if (screen < 1401 && screen > 1280)
        {
          this.rowsNumber = 8;
          this.columnNumber = 9;
        }
        else if (screen < 1281 && screen > 1160)
        {
          this.rowsNumber = 9;
          this.columnNumber = 8;
        }
        else if (screen < 1161 && screen > 1040)
        {
          this.rowsNumber = 10;
          this.columnNumber = 7;
        }
        else if (screen < 1401 && screen > 920)
        {
          this.rowsNumber = 11;
          this.columnNumber = 6;
        }
        else if (screen < 920)
        {
          this.rowsNumber = 14;
          this.columnNumber = 5;
        }
  }

  numSequence(n: number): Array<number> { 
    
    return Array(n); 
    
  } 

}
