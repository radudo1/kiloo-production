import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingelCategoryComponent } from './singel-category.component';

describe('SingelCategoryComponent', () => {
  let component: SingelCategoryComponent;
  let fixture: ComponentFixture<SingelCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingelCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingelCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
