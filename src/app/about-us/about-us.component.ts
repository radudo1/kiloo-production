import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../services/navbar.service';
import { FooterService } from '../services/footer.service';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {
  
  constructor(private nav: NavbarService,
    private footer:FooterService
    ) { }

  ngOnInit(): void {
    this.nav.show();
    this.footer.show();
  }
}
