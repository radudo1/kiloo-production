import { Component, Input, ViewChild, ComponentFactoryResolver, OnDestroy, AfterViewInit, OnInit, ChangeDetectorRef } from '@angular/core';
import { AdDirective } from '../../services/ad.directive';
import { AdItem }      from '../ad-item';
import { AdComponent } from '../ad-interface';

@Component({
  selector: 'app-ad-banner',
  templateUrl: './ad-banner.component.html',
  styleUrls: ['./ad-banner.component.scss']
})
export class AdBannerComponent implements AfterViewInit, OnDestroy {

  @Input() ads: AdItem[];
  currentAddIndex: number = -1;
  @ViewChild(AdDirective) adHost: AdDirective;
  subscription: any;
  interval: any;
  constructor(
    private _componentFactoryResolver: ComponentFactoryResolver,
    private _changeDetectionRef : ChangeDetectorRef
    ) { }
  ngAfterViewInit() {
    this.loadComponent();
    this.getAds();
    this._changeDetectionRef.detectChanges();
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  loadComponent() {
    this.currentAddIndex = (this.currentAddIndex + 1) % this.ads.length;
    let adItem = this.ads[this.currentAddIndex];

    let componentFactory = this._componentFactoryResolver.resolveComponentFactory(adItem.component);

    let viewContainerRef = this.adHost.viewContainerRef;
    viewContainerRef.clear();

    let componentRef = viewContainerRef.createComponent(componentFactory);
    (<AdComponent>componentRef.instance).data = adItem.data;
  }

  getAds() {
    this.interval = setInterval(() => {
      this.loadComponent();
    }, 3000);
  }
}
