import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FullScreenAdComponent } from './full-screen-ad.component';

describe('FullScreenAdComponent', () => {
  let component: FullScreenAdComponent;
  let fixture: ComponentFixture<FullScreenAdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FullScreenAdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FullScreenAdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
