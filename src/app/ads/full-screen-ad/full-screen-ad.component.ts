import { Component, Input, ViewChild, ComponentFactoryResolver, ChangeDetectorRef, AfterViewInit, OnDestroy } from '@angular/core';
import { AdItem } from '../ad-item';
import { AdDirective } from 'src/app/services/ad.directive';
import { AdComponent } from '../ad-interface';

@Component({
  selector: 'app-full-screen-ad',
  templateUrl: './full-screen-ad.component.html',
  styleUrls: ['./full-screen-ad.component.scss']
})
export class FullScreenAdComponent implements AfterViewInit, OnDestroy  {

  @Input() ads: AdItem[];
  currentAddIndex: number = -1;
  @ViewChild(AdDirective) adHost: AdDirective;
  subscription: any;
  interval: any;
  constructor(
    private _componentFactoryResolver: ComponentFactoryResolver,
    private _changeDetectionRef : ChangeDetectorRef
    ) { }
  ngAfterViewInit() {
    this.loadComponent();
    this.getAds();
    this._changeDetectionRef.detectChanges();
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  loadComponent() {
    this.currentAddIndex = (this.currentAddIndex + 1) % this.ads.length;
    let adItem = this.ads[this.currentAddIndex];

    let componentFactory = this._componentFactoryResolver.resolveComponentFactory(adItem.component);

    let viewContainerRef = this.adHost.viewContainerRef;
    viewContainerRef.clear();

    let componentRef = viewContainerRef.createComponent(componentFactory);
    (<AdComponent>componentRef.instance).data = adItem.data;
  }

  getAds() {
    this.interval = setInterval(() => {
      this.loadComponent();
    }, 3000);
  }
}
