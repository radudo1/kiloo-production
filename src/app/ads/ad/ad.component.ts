import { Component, OnInit, HostListener, Renderer2, EventEmitter, Output } from '@angular/core';
import { AdService }         from '../../services/ad.service';
import { AdItem }            from '../ad-item';
@Component({
  selector: 'app-ad',
  templateUrl: './ad.component.html',
  styleUrls: ['./ad.component.scss']
})
export class AdComponent implements OnInit {
  public scroll = false;
  public mouse = false;
  adClosed: boolean = false;
  private timeout: number;
  adheight:number;
  ads: AdItem[];
  @Output() destroyCheck:EventEmitter<string>=new EventEmitter<string>();


  constructor(
    private adService: AdService,
    private renderer: Renderer2) {}

  ngOnInit() {
    this.ads = this.adService.getAds();
  }
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.adClosed = false;
  }
  closeAd(){
    this.adClosed = true;
  }

  @HostListener('window:scroll', ['$event']) // for window scroll events
  @HostListener('mouseover')

  onScroll(event) {
    this.scroll = true;
    const floorAd: HTMLElement = document.getElementById('floorAd--container');

    let floorAdHeight = document.getElementById('floorAd--container').offsetHeight ;
    this.adheight = floorAdHeight;

    if(this.scroll){  
      this.renderer.setStyle(floorAd, 'bottom', '0px');
    }
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.renderer.setStyle(floorAd, 'bottom', `calc(-${floorAdHeight}px*0.4 + 1px)`);
      this.scroll = false;
    }, 1300);

  }
  mouseOver(event){
    this.mouse = true;
    const floorAd: HTMLElement = document.getElementById('floorAd--container');

    let floorAdHeight = document.getElementById('floorAd--container').offsetHeight ;
    this.adheight = floorAdHeight;

    console.log(this.adheight);
    if(this.mouse){  
      this.renderer.setStyle(floorAd, 'bottom', '0px');
    }
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.renderer.setStyle(floorAd, 'bottom', `calc(-${floorAdHeight}px*0.4 + 1px)`);
      this.mouse = false;
    }, 1300);
  }
  remove(elt){
   elt.html('');
  }

}
