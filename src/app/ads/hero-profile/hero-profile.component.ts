import { Component, Input, HostListener, Renderer2 }  from '@angular/core';
import { AdComponent }       from '../ad-interface';
@Component({
  selector: 'app-hero-profile',
  templateUrl: './hero-profile.component.html',
  styleUrls: ['./hero-profile.component.scss']

})
export class HeroProfileComponent implements AdComponent {
  @Input() data: any;


  constructor() {
  }

}