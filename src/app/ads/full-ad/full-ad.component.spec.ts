import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FullAdComponent } from './full-ad.component';

describe('FullAdComponent', () => {
  let component: FullAdComponent;
  let fixture: ComponentFixture<FullAdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FullAdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FullAdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
