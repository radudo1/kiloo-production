import { Component, OnInit } from '@angular/core';
import { AdService }         from '../../services/ad.service';
import { AdItem }            from '../ad-item';

@Component({
  selector: 'app-full-ad',
  templateUrl: './full-ad.component.html',
  styleUrls: ['./full-ad.component.scss']
})
export class FullAdComponent implements OnInit {
  public scroll = false;
  public mouse = false;
  adClosed: boolean = false;
  private timeout: number;
  adheight:number;
  ads: AdItem[];


  constructor(
    private adService: AdService) {}

  ngOnInit() {
    this.ads = this.adService.getAds();
  }
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.adClosed = false;
  }
  closeAd(){
    this.adClosed = true;
  }




}
