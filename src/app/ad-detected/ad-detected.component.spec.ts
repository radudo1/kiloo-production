import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdDetectedComponent } from './ad-detected.component';

describe('AdDetectedComponent', () => {
  let component: AdDetectedComponent;
  let fixture: ComponentFixture<AdDetectedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdDetectedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdDetectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
