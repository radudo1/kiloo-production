import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-ad-detected',
  templateUrl: './ad-detected.component.html',
  styleUrls: ['./ad-detected.component.scss']
})
export class AdDetectedComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  refresh(): void {
    window.location.reload();
  }
  @Output() onHide = new EventEmitter<boolean>();
  setHide(){
     this.onHide.emit(false);
  }


}
