import { Component, OnInit, OnDestroy } from '@angular/core';
import { HostListener } from "@angular/core";
import { AuthService } from '../services/auth.service';
import { NavbarService } from '../services/navbar.service';
import { FooterService } from '../services/footer.service';
import { Router } from '@angular/router';
import { RatingService } from '../services/rating.service';



@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],

})
export class MainComponent implements OnInit {

  screenWidth: number;
  gridWidth: number;
  loadComponent2x3 = false;
  loadComponent3x3 = true;



  constructor(
    private nav: NavbarService,
    private footer: FooterService,
    private router: Router,
    public authService: AuthService,
    private rating: RatingService,

    ) {
      this.getScreenSize();
  }

  ngOnInit(){
    this.nav.show();
    this.footer.show();
    this.rating.hide();

  }

  processChange(e){
     if(e && this.loadComponent2x3){
      !this.loadComponent3x3;
    }else{
      !this.loadComponent3x3;
    }
  }

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
        this.screenWidth = window.innerWidth;
        if (this.screenWidth <= 1058){
          window.resizeTo(1058,window.innerHeight)
        }

  }


  

}
