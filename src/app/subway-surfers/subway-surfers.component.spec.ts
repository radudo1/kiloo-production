import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubwaySurfersComponent } from './subway-surfers.component';

describe('SubwaySurfersComponent', () => {
  let component: SubwaySurfersComponent;
  let fixture: ComponentFixture<SubwaySurfersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubwaySurfersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubwaySurfersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
