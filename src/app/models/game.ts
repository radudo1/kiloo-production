export class Game {
    name: string;
    developer: string;
    likes: number;
    dislikes: number;
    iframeLink: string;
    pictureLink: string;
}
