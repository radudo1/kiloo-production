import { Component, OnInit, HostListener, Inject, Renderer2 } from '@angular/core';
import { NavbarService } from '../services/navbar.service';
import { FooterService } from '../services/footer.service';
import { Game } from '../models/game';
import {SafePipe} from '../safe.pipe';
import { Position } from '../models/position';
import { NavGameService } from '../services/nav-game.service';
import { DOCUMENT } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-frisbee-forever',
  templateUrl: './frisbee-forever.component.html',
  styleUrls: ['./frisbee-forever.component.scss']
})
export class FrisbeeForeverComponent implements OnInit {
 ///----------------------------------///
 id: string;
 screenWidth: number;
 isFullScreen: boolean;
 elem: any;
 bottom: any;
 rowsNumber:number;
 columnNumber:number;
 gridPos;
 liked: boolean;
 disliked: boolean;
 game: Game;
 playAgainC;
 playAgainR;
 clicked: boolean = false;
 timeout:boolean ;
 play:boolean = false;
 playClicked: boolean = false;
///---------------------------------///

   constructor(
     private nav: NavbarService,
     private navGame: NavGameService,
     @Inject(DOCUMENT) private document: any,
     private footer: FooterService,
     public router:Router,
     private activatedRoute: ActivatedRoute,
     private renderer: Renderer2

   ) {}

   ngOnInit(): void {
     this.nav.hide();
     this.navGame.show();
     this.footer.show();
     this.elem = document.getElementById('iFrameGame');
     this.bottom = document.getElementById('iFrameBottom');
     this.getScreenSize();
     this.id = this.activatedRoute.snapshot.paramMap.get('id');
     this.chkScreenMode();
     this.game = { name:'Frisbee Forever', developer: 'Kiloo', likes: 156, dislikes: 89, pictureLink:'../../assets/img/grid/real_thumbnails/9.png',iframeLink:'https://www.kiloo.com/storage/frisbee-forever-2-no-post-processing/html5'};
   }

   ngOnDestroy(): void {
     //Called once, before the instance is destroyed.
     //Add 'implements OnDestroy' to the class.
     this.navGame.hide();
   }

   @HostListener('document:fullscreenchange', ['$event'])
   @HostListener('document:webkitfullscreenchange', ['$event'])
   @HostListener('document:mozfullscreenchange', ['$event'])
   @HostListener('document:MSFullscreenChange', ['$event'])

   fullscreenmodes(event){
     this.chkScreenMode();
   }
   playClick(){
     this.clicked = true;
   }
   chkScreenMode(){
     if(document.fullscreenElement){
       //fullscreen
       this.isFullScreen = true;
       const iframe: HTMLElement = document.getElementById('iFrame');
       this.renderer.setStyle(iframe, 'height', 'calc(100% - 68px)');
     }else{
       //not in full screen
       this.isFullScreen = false;
       const iframe: HTMLElement = document.getElementById('iFrame');
       this.renderer.setStyle(iframe, 'height', 'calc(100%)');
     }
   }

   @HostListener('window:resize', ['$event'])
   getScreenSize(event?) {
       this.screenWidth = window.innerWidth; 
       let screen = this.screenWidth;
       this.gridPos = new Position();



       if ( screen > 1500) 
       {
         this.rowsNumber = 7;
         this.columnNumber = 11;
         this.gridPos.x =  "auto / span 6";
         this.gridPos.y =  "auto / span 6";
       } 
       else if (screen < 1501 && screen > 1400)
       {
         this.rowsNumber = 7;
         this.columnNumber = 10;
         this.gridPos.x =  "3 / span 6";
         this.gridPos.y =  "1 / span 6";

       }
       else if (screen < 1401 && screen > 1280)
       {
         this.rowsNumber = 9;
         this.columnNumber = 9;
         this.gridPos.x =  "2 / span 6";
         this.gridPos.y =  "1 / span 6";

       }
       else if (screen < 1281 && screen > 1160)
       {
         this.rowsNumber = 10;
         this.columnNumber = 8;
         this.gridPos.x =  "2 / span 6";
         this.gridPos.y =  "1 / span 6";
       }
       else if (screen < 1161 && screen > 1040)
       {
         this.rowsNumber = 11;
         this.columnNumber = 7;
         this.elem.focus();
         this.gridPos.x =  "1 / span 6";
         this.gridPos.y =  "1 / span 6";
       }
       else if (screen < 1401 )
       {
         this.rowsNumber = 13;
         this.columnNumber = 6;
         this.elem.focus();
         this.gridPos.x =  "1 / span 6";
         this.gridPos.y =  "1 / span 6";
       }

 }

     onLike(game) {
       
 
         if(!this.liked && !this.disliked){
           this.liked = true;
           this.game.likes++;
         } else if (this.liked && !this.disliked){
           this.liked = false;
           this.game.likes = this.game.likes - 1;
         }
     }

     onDislike(game) {
          
       if(!this.liked && !this.disliked){
         this.disliked = true;
         this.game.dislikes++;
       } else if (!this.liked && this.disliked){
       this.disliked = false;
       this.game.dislikes = this.game.dislikes - 1;
     }

   }
   
   numSequence(n: number): Array<number> { 
     return Array(n); 
   } 
   openFullscreen() {
     if (this.elem.requestFullscreen) {
       this.elem.requestFullscreen();
     } else if (this.elem.mozRequestFullScreen) {
       /* Firefox */
       this.elem.mozRequestFullScreen();
     } else if (this.elem.webkitRequestFullscreen) {
       /* Chrome, Safari and Opera */
       this.elem.webkitRequestFullscreen();
     } else if (this.elem.msRequestFullscreen) {
       /* IE/Edge */
       this.elem.msRequestFullscreen();
     }
   }
 /* Close fullscreen */
   closeFullscreen() {
     if (this.document.exitFullscreen) {
       this.document.exitFullscreen();
     } else if (this.document.mozCancelFullScreen) {
       /* Firefox */
       this.document.mozCancelFullScreen();
     } else if (this.document.webkitExitFullscreen) {
       /* Chrome, Safari and Opera */
       this.document.webkitExitFullscreen();
     } else if (this.document.msExitFullscreen) {
       /* IE/Edge */
       this.document.msExitFullscreen();
     }
   }


 



}
