import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrisbeeForeverComponent } from './frisbee-forever.component';

describe('FrisbeeForeverComponent', () => {
  let component: FrisbeeForeverComponent;
  let fixture: ComponentFixture<FrisbeeForeverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrisbeeForeverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrisbeeForeverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
