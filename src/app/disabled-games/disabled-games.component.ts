import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-disabled-games',
  templateUrl: './disabled-games.component.html',
  styleUrls: ['./disabled-games.component.scss']
})
export class DisabledGamesComponent implements OnInit {

  screenWidth: number;
  rowsNumber:number;
  columnNumber: number;
  disabled:boolean;

  constructor() { }

  ngOnInit(): void {
    this.getScreenSize();
    this.disabled=true;
  }
  dim(bool) {
    document.getElementById('dimmer').style.display=(bool?'block':'none');
  }
  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
        this.screenWidth = window.innerWidth; 
        let screen = this.screenWidth;

        if ( screen > 1500) 
        {
          this.columnNumber = 11;
        } 
        else if (screen < 1501 && screen > 1400)
        {
          this.columnNumber = 10;
        }
        else if (screen < 1401 && screen > 1280)
        {
          this.columnNumber = 9;
        }
        else if (screen < 1281 && screen > 1160)
        {
          this.columnNumber = 8;
        }
        else if (screen < 1161 && screen > 1040)
        {
          this.columnNumber = 7;
        }
        else if (screen < 1401 && screen > 920)
        {
          this.columnNumber = 6;
        }
        else if (screen < 920)
        {
          this.columnNumber = 5;
        }
  }
  numSequence(n: number): Array<number> { 
    
    return Array(n); 
    
  } 
}
