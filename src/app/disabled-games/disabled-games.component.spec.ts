import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisabledGamesComponent } from './disabled-games.component';

describe('DisabledGamesComponent', () => {
  let component: DisabledGamesComponent;
  let fixture: ComponentFixture<DisabledGamesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisabledGamesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisabledGamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
