import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../services/navbar.service';
import { FooterService } from '../services/footer.service';
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss']
})
export class SearchResultsComponent implements OnInit {
  searchCriteria: any;
  constructor(
    private nav: NavbarService,
    private fot: FooterService,
    private router:Router,
    private sharingService:DataService
    ) { }

  ngOnInit(): void {
    this.nav.show();
    this.fot.show();
    this.searchCriteria = this.sharingService.getData();
  }
  numSequence(n: number): Array<number> { 
    
    return Array(n); 
    
  } 
  

}
