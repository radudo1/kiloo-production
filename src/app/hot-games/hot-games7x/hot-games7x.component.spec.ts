import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotGames7xComponent } from './hot-games7x.component';

describe('HotGames7xComponent', () => {
  let component: HotGames7xComponent;
  let fixture: ComponentFixture<HotGames7xComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotGames7xComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotGames7xComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
