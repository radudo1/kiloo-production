import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotGames10xComponent } from './hot-games10x.component';

describe('HotGames10xComponent', () => {
  let component: HotGames10xComponent;
  let fixture: ComponentFixture<HotGames10xComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotGames10xComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotGames10xComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
