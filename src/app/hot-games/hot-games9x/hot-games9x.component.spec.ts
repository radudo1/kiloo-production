import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotGames9xComponent } from './hot-games9x.component';

describe('HotGames9xComponent', () => {
  let component: HotGames9xComponent;
  let fixture: ComponentFixture<HotGames9xComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotGames9xComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotGames9xComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
