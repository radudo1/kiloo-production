import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotGames6xComponent } from './hot-games6x.component';

describe('HotGames6xComponent', () => {
  let component: HotGames6xComponent;
  let fixture: ComponentFixture<HotGames6xComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotGames6xComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotGames6xComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
