import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotGames5xComponent } from './hot-games5x.component';

describe('HotGames5xComponent', () => {
  let component: HotGames5xComponent;
  let fixture: ComponentFixture<HotGames5xComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotGames5xComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotGames5xComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
