import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotGames11xComponent } from './hot-games11x.component';

describe('HotGames11xComponent', () => {
  let component: HotGames11xComponent;
  let fixture: ComponentFixture<HotGames11xComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotGames11xComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotGames11xComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
