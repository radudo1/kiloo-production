import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotGames8xComponent } from './hot-games8x.component';

describe('HotGames8xComponent', () => {
  let component: HotGames8xComponent;
  let fixture: ComponentFixture<HotGames8xComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotGames8xComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotGames8xComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
