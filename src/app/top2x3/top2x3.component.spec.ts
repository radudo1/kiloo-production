import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Top2x3Component } from './top2x3.component';

describe('Top2x3Component', () => {
  let component: Top2x3Component;
  let fixture: ComponentFixture<Top2x3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Top2x3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Top2x3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
