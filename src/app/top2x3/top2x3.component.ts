import { Component, OnInit, HostListener, Renderer2, ViewChild, ElementRef } from '@angular/core';
import { NavbarService } from '../services/navbar.service';
import { FooterService } from '../services/footer.service';
import { Position } from '../models/position';

@Component({
  selector: 'app-top2x3',
  templateUrl: './top2x3.component.html',
  styleUrls: ['./top2x3.component.scss']
})
export class Top2x3Component implements OnInit {
  video1On: boolean = false;
  video2On: boolean = false;
  video3On: boolean = false;
  screenWidth: number;
  icon1;
  icon2;
  icon3;
  twoByThree;
  twoPosition;
  threePosition;
  fourPosition;
  fivePosition;
  sixPosition;
  sevenPosition;
  eightPosition;
  ninePosition;
  tenPosition;
  elevenPosition;
  twelvePosition;
  thirteenPosition;
  fourteenPosition;
  fifteenPosition;
  sixteenPosition;
  seventeenPosition;
  eighteenPosition;
  ninteenPosition;
  twentyPosition;

  rowsNumber: number;
  columnNumber: number;
  constructor(
    private renderer: Renderer2,
    private nav: NavbarService,
    private foot: FooterService
  ) { }

  ngOnInit(): void {
    this.getScreenSize();
    this.nav.show();
    this.foot.show();
  }

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
        this.screenWidth = window.innerWidth; 
        let screen = this.screenWidth;

        //Screen Positions for icons
        this.twoByThree = new Position();
        this.icon1 = new Position();
        this.icon2 = new Position();
        this.icon3 = new Position();

        this.twoPosition = new Position();
        this.threePosition = new Position();
        this.fourPosition = new Position();
        this.fivePosition = new Position();
        this.sixPosition = new Position();
        this.sevenPosition = new Position();
        this.eightPosition = new Position();
        this.ninePosition = new Position();
        this.tenPosition = new Position();
        this.elevenPosition = new Position();
        this.twelvePosition = new Position();
        this.thirteenPosition = new Position();
        this.fourteenPosition = new Position();
        this.fifteenPosition = new Position();
        this.sixteenPosition = new Position();
        this.seventeenPosition = new Position();
        this.eighteenPosition = new Position();
        this.ninteenPosition = new Position();
        this.twentyPosition = new Position();

        if ( screen > 1500) 
        {
          this.columnNumber = 11;
          this.rowsNumber = 4;
          //Big Promo          
          this.twoByThree.x =  "auto / span 3";
          this.twoByThree.y =  "auto / span 2";
          //First Icon
          this.icon1.x =  " 3 / span 1";
          this.icon1.y = " 1 / span 1";
          //Second Poisition
          this.twoPosition.x =  "4 / span 2";
          this.twoPosition.y =  "1 / span 1";
          //Third Poisition
          this.threePosition.x =  "6 / span 2";
          this.threePosition.y =  "1 / span 2";          
          //Fourth Poisition
          this.fourPosition.x =  "8 / span 1";
          this.fourPosition.y =  "1 / span 1"; 
          //Fifth Poisition
          this.fivePosition.x =  "3 / span 1";
          this.fivePosition.y =  "2 / span 1"; 
          //Sixth Poisition
          this.sixPosition.x =  "4 / span 2";
          this.sixPosition.y =  "2 / span 2";
          //Seventh Poisition
          this.sevenPosition.x =  "8 / span 1";
          this.sevenPosition.y =  "2 / span 1";
          //Eight Poisition
          this.eightPosition.x =  "1 / span 2";
          this.eightPosition.y =  "3 / span 1";   
          //Icon2 Poisition
          this.icon2.x =  "3 / span 1";
          this.icon2.y =  "3 / span 1";    
          //Icon3 Poisition
          this.icon3.x =  "6 / span 1";
          this.icon3.y =  "3 / span 1";    
          //Ninth Poisition
          this.ninePosition.x =  "7 / span 1";
          this.ninePosition.y =  "3 / span 1";  
          //Ten Poisition
          this.tenPosition.x =  "8 / span 1";
          this.tenPosition.y =  "3 / span 1";  
          //Elventh Poisition
          this.elevenPosition.x =  "1 / span 2";
          this.elevenPosition.y =  "4 / span 1";    
          //Twelveth Poisition
          this.twelvePosition.x =  "3 / span 1";
          this.twelvePosition.y =  "4 / span 1"; 
          //Thirteen Poisition
          this.thirteenPosition.x =  "4 / span 1";
          this.thirteenPosition.y =  "4 / span 1";    
          //Fourteen Poisition
          this.fourteenPosition.x =  "5 / span 1";
          this.fourteenPosition.y =  "4 / span 1";   
          //Fifteen Poisition
          this.fifteenPosition.x =  "6 / span 1";
          this.fifteenPosition.y =  "4 / span 1";   
          //Sixteen Poisition
          this.sixteenPosition.x =  "7 / span 2";
          this.sixteenPosition.y =  "4 / span 1";    
          //Seventeen Poisition
          this.seventeenPosition.x =  "9 / span 1";
          this.seventeenPosition.y =  "4 / span 1";      
          //Eighteen Poisition
          this.eighteenPosition.x =  "10 / span 1";
          this.eighteenPosition.y =  "4 / span 1";     
          //Nineteen Poisition
          this.ninteenPosition.x =  "11 / span 1";
          this.ninteenPosition.y =  "4 / span 1";     
        } 
        else if (screen < 1501 && screen > 1400)
        {
          this.rowsNumber = 5;
          this.columnNumber = 10;
          //Big Promo          
          this.twoByThree.x =  "8 / span 3";
          this.twoByThree.y =  "1 / span 3";
          //First Icon
          this.icon1.x =  " 3 / span 1";
          this.icon1.y = " 1 / span 1";          
          //Second Poisition
          this.twoPosition.x =  "4 / span 2";
          this.twoPosition.y =  "1 / span 1";
          //Third Poisition
          this.threePosition.x =  "6 / span 2";
          this.threePosition.y =  "1 / span 2";  
          //Fourth Poisition
          this.fourPosition.x =  "9 / span 1";
          this.fourPosition.y =  "4 / span 1"; 
          //Fifth Poisition
          this.fivePosition.x =  "3 / span 1";
          this.fivePosition.y =  "2 / span 1";
          //Sixth Poisition
          this.sixPosition.x =  "4 / span 2";
          this.sixPosition.y =  "2 / span 2"; 
          //Seventh Poisition
          this.sevenPosition.x =  "10 / span 1";
          this.sevenPosition.y =  "4 / span 1";
          //Eight Poisition
          this.eightPosition.x =  "1 / span 2";
          this.eightPosition.y =  "3 / span 1"; 
          //Icon2 Poisition
          this.icon2.x =  "3 / span 1";
          this.icon2.y =  "3 / span 1";    
          //Icon3 Poisition
          this.icon3.x =  "6 / span 1";
          this.icon3.y =  "3 / span 1";   
          //Ninth Poisition
          this.ninePosition.x =  "7 / span 1";
          this.ninePosition.y =  "3 / span 1"; 
          //Ten Poisition
          this.tenPosition.x =  "1 / span 1";
          this.tenPosition.y =  "5 / span 1"; 
          //Elventh Poisition
          this.elevenPosition.x =  "1 / span 2";
          this.elevenPosition.y =  "4 / span 1";
          //Twelveth Poisition
          this.twelvePosition.x =  "3 / span 1";
          this.twelvePosition.y =  "4 / span 1";  
          //Thirteen Poisition
          this.thirteenPosition.x =  "4 / span 1";
          this.thirteenPosition.y =  "4 / span 1";  
          //Fourteen Poisition
          this.fourteenPosition.x =  "5 / span 1";
          this.fourteenPosition.y =  "4 / span 1";   
          //Fifteen Poisition
          this.fifteenPosition.x =  "6/ span 1";
          this.fifteenPosition.y =  "4 / span 1";  
          //Sixteen Poisition
          this.sixteenPosition.x =  "7 / span 2";
          this.sixteenPosition.y =  "4 / span 1";  
          //Seventeen Poisition
          this.seventeenPosition.x =  "2 / span 1";
          this.seventeenPosition.y =  "5 / span 1";  
          //Eighteen Poisition
          this.eighteenPosition.x =  "3 / span 1";
          this.eighteenPosition.y =  "5 / span 1";   
          //Nineteen Poisition
          this.ninteenPosition.x =  "4 / span 1";
          this.ninteenPosition.y =  "5 / span 1";     

        }
        else if (screen < 1401 && screen > 1280)
        {
          this.rowsNumber = 5;
          this.columnNumber = 9;
          //Big Promo          
          this.twoByThree.x =  "7 / span 3";
          this.twoByThree.y =  "1 / span 3";
          //First Icon
          this.icon1.x =  " 3 / span 1";
          this.icon1.y = " 1 / span 1";          
          //Second Poisition
          this.twoPosition.x =  "4 / span 2";
          this.twoPosition.y =  "1 / span 1";
          //Third Poisition
          this.threePosition.x =  "6 / span 2";
          this.threePosition.y =  "4 / span 2";  
          //Fourth Poisition
          this.fourPosition.x =  "6 / span 1";
          this.fourPosition.y =  "3 / span 1"; 
          //Fifth Poisition
          this.fivePosition.x =  "3 / span 1";
          this.fivePosition.y =  "2 / span 1"; 
          //Sixth Poisition
          this.sixPosition.x =  "4 / span 2";
          this.sixPosition.y =  "2 / span 2"; 
          //Seventh Poisition
          this.sevenPosition.x =  "1 / span 1";
          this.sevenPosition.y =  "5 / span 1";    
          //Eight Poisition
          this.eightPosition.x =  "1 / span 2";
          this.eightPosition.y =  "3 / span 1";
          //Icon2 Poisition
          this.icon2.x =  "3 / span 1";
          this.icon2.y =  "3 / span 1"; 
          //Icon3 Poisition
          this.icon3.x =  "5 / span 1";
          this.icon3.y =  "5 / span 1";  
          //Ninth Poisition
          this.ninePosition.x =  "6 / span 1";
          this.ninePosition.y =  "2 / span 1";   
          //Ten Poisition
          this.tenPosition.x =  "2 / span 1";
          this.tenPosition.y =  "5 / span 1"; 
          //Elventh Poisition
          this.elevenPosition.x =  "1 / span 2";
          this.elevenPosition.y =  "4 / span 1";
          //Twelveth Poisition
          this.twelvePosition.x =  "3 / span 1";
          this.twelvePosition.y =  "4 / span 1"; 
          //Thirteen Poisition
          this.thirteenPosition.x =  "4 / span 1";
          this.thirteenPosition.y =  "4 / span 1";
          //Fourteen Poisition
          this.fourteenPosition.x =  "5 / span 1";
          this.fourteenPosition.y =  "4 / span 1"; 
          //Fifteen Poisition
          this.fifteenPosition.x =  "6 / span 1";
          this.fifteenPosition.y =  "1 / span 1"; 
          //Sixteen Poisition
          this.sixteenPosition.x =  "8 / span 2";
          this.sixteenPosition.y =  "4 / span 1"; 
          //Seventeen Poisition
          this.seventeenPosition.x =  "3 / span 1";
          this.seventeenPosition.y =  "5 / span 1";
          //Eighteen Poisition
          this.eighteenPosition.x =  "8 / span 1";
          this.eighteenPosition.y =  "5 / span 1";
          //Nineteen Poisition
          this.ninteenPosition.x =  "4 / span 1";
          this.ninteenPosition.y =  "5 / span 1";     
                                    
        }
        else if (screen < 1281 && screen > 1160)
        {
          this.rowsNumber = 6;
          this.columnNumber = 8;
          //Big Promo          
          this.twoByThree.x =  "6 / span 3";
          this.twoByThree.y =  "1 / span 3";
          //First Icon
          this.icon1.x =  " 3 / span 1";
          this.icon1.y = " 1 / span 1";          
          //Second Poisition
          this.twoPosition.x =  "4 / span 2";
          this.twoPosition.y =  "1 / span 1";
          //Third Poisition
          this.threePosition.x =  "6 / span 2";
          this.threePosition.y =  "4 / span 2"; 
          //Fourth Poisition
          this.fourPosition.x =  "8 / span 1";
          this.fourPosition.y =  "4 / span 1"; 
          //Fifth Poisition
          this.fivePosition.x =  "3 / span 1";
          this.fivePosition.y =  "2 / span 1"; 
          //Sixth Poisition
          this.sixPosition.x =  "4 / span 2";
          this.sixPosition.y =  "2 / span 2";   
          //Seventh Poisition
          this.sevenPosition.x =  "8 / span 1";
          this.sevenPosition.y =  "5 / span 1";
          //Eight Poisition
          this.eightPosition.x =  "1 / span 2";
          this.eightPosition.y =  "3 / span 1";
          //Icon2 Poisition
          this.icon2.x =  "3 / span 1";
          this.icon2.y =  "3 / span 1";
          //Icon3 Poisition
          this.icon3.x =  "5 / span 1";
          this.icon3.y =  "5 / span 1";   
          //Nineth Poisition
          this.ninePosition.x =  "2 / span 1";
          this.ninePosition.y =  "5 / span 1"; 
          //Ten Poisition
          this.tenPosition.x =  "1 / span 1";
          this.tenPosition.y =  "6 / span 1"; 
          //Elventh Poisition
          this.elevenPosition.x =  "1 / span 2";
          this.elevenPosition.y =  "4 / span 1"; 
          //Twelveth Poisition
          this.twelvePosition.x =  "3 / span 1";
          this.twelvePosition.y =  "4 / span 1";
          //Thirteen Poisition
          this.thirteenPosition.x =  "4 / span 1";
          this.thirteenPosition.y =  "4 / span 1";
          //Fourteen Poisition
          this.fourteenPosition.x =  "5 / span 1";
          this.fourteenPosition.y =  "4 / span 1";
          //Fifteen Poisition
          this.fifteenPosition.x =  "1 / span 1";
          this.fifteenPosition.y =  "5 / span 1";
          //Sixteen Poisition
          this.sixteenPosition.x =  "3 / span 2";
          this.sixteenPosition.y =  "5 / span 1"; 
          //Seventeen Poisition
          this.seventeenPosition.x =  "2 / span 1";
          this.seventeenPosition.y =  "6 / span 1";
          //Eighteen Poisition
          this.eighteenPosition.x =  "4 / span 1";
          this.eighteenPosition.y =  "6 / span 1"; 
          //Nineteen Poisition
          this.ninteenPosition.x =  "3 / span 1";
          this.ninteenPosition.y =  "6 / span 1";                               
        }
        else if (screen < 1161 && screen > 1040)
        {
          this.rowsNumber = 7;
          this.columnNumber = 7;
          //Big Promo          
          this.twoByThree.x =  "5 / span 3";
          this.twoByThree.y =  "1 / span 3";
          //First Icon
          this.icon1.x =  " 3 / span 1";
          this.icon1.y = " 1 / span 1";          
          //Second Poisition
          this.twoPosition.x =  "4 / span 2";
          this.twoPosition.y =  "4 / span 1";
          //Third Poisition
          this.threePosition.x =  "6 / span 2";
          this.threePosition.y =  "4 / span 2"; 
          //Fourth Poisition
          this.fourPosition.x =  "2 / span 1";
          this.fourPosition.y =  "6 / span 1";
          //Fifth Poisition
          this.fivePosition.x =  "3 / span 1";
          this.fivePosition.y =  "2 / span 1"; 
          //Sixth Poisition
          this.sixPosition.x =  "1 / span 2";
          this.sixPosition.y =  "4 / span 2"; 
          //Seventh Poisition
          this.sevenPosition.x =  "5 / span 1";
          this.sevenPosition.y =  "6 / span 1"; 
          //Eight Poisition
          this.eightPosition.x =  "1 / span 2";
          this.eightPosition.y =  "3 / span 1"; 
          //Icon2 Poisition
          this.icon2.x =  "5 / span 1";
          this.icon2.y =  "5 / span 1"; 
          //Icon3 Poisition
          this.icon3.x =  "3/ span 1";
          this.icon3.y =  "4 / span 1";    
          //Nineth Poisition
          this.ninePosition.x =  "1 / span 1";
          this.ninePosition.y =  "6 / span 1";  
          //Ten Poisition
          this.tenPosition.x =  "6 / span 1";
          this.tenPosition.y =  "6 / span 1"; 
          //Elventh Poisition
          this.elevenPosition.x =  "3 / span 2";
          this.elevenPosition.y =  "3 / span 1";
          //Twelveth Poisition
          this.twelvePosition.x =  "4 / span 1";
          this.twelvePosition.y =  "1 / span 1";
          //Thirteen Poisition
          this.thirteenPosition.x =  "4 / span 1";
          this.thirteenPosition.y =  "2 / span 1"; 
          //Fourteen Poisition
          this.fourteenPosition.x =  "3 / span 1";
          this.fourteenPosition.y =  "5 / span 1";
          //Fifteen Poisition
          this.fifteenPosition.x =  "4 / span 1";
          this.fifteenPosition.y =  "5 / span 1";
          //Sixteen Poisition
          this.sixteenPosition.x =  "3 / span 2";
          this.sixteenPosition.y =  "6 / span 1"; 
          //Seventeen Poisition
          this.seventeenPosition.x =  "7 / span 1";
          this.seventeenPosition.y =  "6 / span 1";  
          //Eighteen Poisition
          this.eighteenPosition.x =  "2 / span 1";
          this.eighteenPosition.y =  "7 / span 1"; 
          //Nineteen Poisition
          this.ninteenPosition.x =  "1 / span 1";
          this.ninteenPosition.y =  "7 / span 1";  
        }
        else if (screen < 1401 && screen > 920)
        {
          this.rowsNumber = 8;
          this.columnNumber = 6;
          //Big Promo          
          this.twoByThree.x =  "4 / span 3";
          this.twoByThree.y =  "1 / span 3";
          //First Icon
          this.icon1.x =  " 3 / span 1";
          this.icon1.y = " 1 / span 1";          
          //Second Poisition
          this.twoPosition.x =  "5 / span 2";
          this.twoPosition.y =  "6 / span 1";
          //Third Poisition
          this.threePosition.x =  "4 / span 2";
          this.threePosition.y =  "4 / span 2";  
          //Fourth Poisition
          this.fourPosition.x =  "1 / span 1";
          this.fourPosition.y =  "7 / span 1"; 
          //Fifth Poisition
          this.fivePosition.x =  "3 / span 1";
          this.fivePosition.y =  "2 / span 1";  
          //Sixth Poisition
          this.sixPosition.x =  "1 / span 2";
          this.sixPosition.y =  "4 / span 2"; 
          //Seventh Poisition
          this.sevenPosition.x =  "4 / span 1";
          this.sevenPosition.y =  "7 / span 1";   
          //Eight Poisition
          this.eightPosition.x =  "1 / span 2";
          this.eightPosition.y =  "3 / span 1";  
          //Icon2 Poisition
          this.icon2.x =  "6 / span 1";
          this.icon2.y =  "5 / span 1"; 
          //Icon3 Poisition
          this.icon3.x =  "3 / span 1";
          this.icon3.y =  "4 / span 1";
          //Nineth Poisition
          this.ninePosition.x =  "4 / span 1";
          this.ninePosition.y =  "6 / span 1"; 
          //Ten Poisition
          this.tenPosition.x =  "5 / span 1";
          this.tenPosition.y =  "7 / span 1";  
          //Elventh Poisition
          this.elevenPosition.x =  "1 / span 2";
          this.elevenPosition.y =  "6 / span 1";
          //Twelveth Poisition
          this.twelvePosition.x =  "3 / span 1";
          this.twelvePosition.y =  "3 / span 1";
          //Thirteen Poisition
          this.thirteenPosition.x =  "6 / span 1";
          this.thirteenPosition.y =  "4 / span 1"; 
          //Fourteen Poisition
          this.fourteenPosition.x =  "3 / span 1";
          this.fourteenPosition.y =  "5 / span 1";
          //Fifteen Poisition
          this.fifteenPosition.x =  "3 / span 1";
          this.fifteenPosition.y =  "6 / span 1";  
          //Sixteen Poisition
          this.sixteenPosition.x =  "2 / span 2";
          this.sixteenPosition.y =  "7 / span 1";
          //Seventeen Poisition
          this.seventeenPosition.x =  "6 / span 1";
          this.seventeenPosition.y =  "7 / span 1";  
          //Eighteen Poisition
          this.eighteenPosition.x =  "2 / span 1";
          this.eighteenPosition.y =  "8 / span 1";
          //Nineteen Poisition
          this.ninteenPosition.x =  "1 / span 1";
          this.ninteenPosition.y =  "8 / span 1";                                                           
        }
        else if (screen < 920)
        {
          this.rowsNumber = 9;
          this.columnNumber = 5;
          //Big Promo
          this.twoByThree.x =  "3 / span 3";
          this.twoByThree.y =  "1 / span 3";
          //First Icon
          this.icon1.x =  " 1 / span 1";
          this.icon1.y = " 3 / span 1";             
          //Second Poisition
          this.twoPosition.x =  "4 / span 2";
          this.twoPosition.y =  "7 / span 1";
          //Third Poisition
          this.threePosition.x =  "4 / span 2";
          this.threePosition.y =  "4 / span 2";  
          //Fourth Poisition
          this.fourPosition.x =  "2 / span 1";
          this.fourPosition.y =  "8 / span 1";
          //Fifth Poisition
          this.fivePosition.x =  "2 / span 1";
          this.fivePosition.y =  "3 / span 1"; 
          //Sixth Poisition
          this.sixPosition.x =  "1 / span 2";
          this.sixPosition.y =  "4 / span 2";    
          //Seventh Poisition
          this.sevenPosition.x =  "5 / span 1";
          this.sevenPosition.y =  "8 / span 1";
          //Eight Poisition
          this.eightPosition.x =  "2 / span 2";
          this.eightPosition.y =  "6 / span 1";
          //Icon2 Poisition
          this.icon2.x =  "5 / span 1";
          this.icon2.y =  "6 / span 1";
          //Icon3 Poisition
          this.icon3.x =  "3 / span 1";
          this.icon3.y =  "4 / span 1";  
          //Nineth Poisition
          this.ninePosition.x =  "1 / span 1";
          this.ninePosition.y =  "8 / span 1";   
          //Ten Poisition
          this.tenPosition.x =  "1 / span 1";
          this.tenPosition.y =  "9 / span 1"; 
          //Elventh Poisition
          this.elevenPosition.x =  "1 / span 2";
          this.elevenPosition.y =  "7 / span 1"; 
          //Twelveth Poisition
          this.twelvePosition.x =  "1 / span 1";
          this.twelvePosition.y =  "6 / span 1";
          //Thirteen Poisition
          this.thirteenPosition.x =  "4 / span 1";
          this.thirteenPosition.y =  "6 / span 1";
          //Fourteen Poisition
          this.fourteenPosition.x =  "3 / span 1";
          this.fourteenPosition.y =  "5 / span 1";
          //Fifteen Poisition
          this.fifteenPosition.x =  "3 / span 1";
          this.fifteenPosition.y =  "7 / span 1";  
          //Sixteen Poisition
          this.sixteenPosition.x =  "3 / span 2";
          this.sixteenPosition.y =  "8 / span 1";
          //Seventeen Poisition
          this.seventeenPosition.x =  "2 / span 1";
          this.seventeenPosition.y =  "9 / span 1";  
          //Eighteen Poisition
          this.eighteenPosition.x =  "4 / span 1";
          this.eighteenPosition.y =  "9 / span 1";  
          //Nineteen Poisition
          this.ninteenPosition.x =  "3 / span 1";
          this.ninteenPosition.y =  "9 / span 1";                        
        }
  }
  
  numSequence(n: number): Array<number> { 
    return Array(n); 
  } 

  @ViewChild('videoPlayer') videoplayer: ElementRef;
  @ViewChild('videoPlayer2') videoplayer2: ElementRef;
  @ViewChild('videoPlayer3') videoplayer3: ElementRef;
  @ViewChild('videoPlayerEditor') videoPlayerEditor: ElementRef;
  
  toggleVideo(event: any) {
    this.video1On = true;
    let  videoPlayer = this.videoplayer.nativeElement;
    this.renderer.setStyle(videoPlayer, 'opacity', '1' );
    this.renderer.setStyle(videoPlayer,  'transition', '0.3s  ease-in-out 0.3s ');
    videoPlayer.play();

  }
  toggleVideo2(event: any) {
    this.video2On = true;
    let  videoPlayer = this.videoplayer2.nativeElement;
     this.renderer.setStyle(videoPlayer, 'opacity', '1' );
     this.renderer.setStyle(videoPlayer,  'transition', '0.3s  ease-in-out 0.3s ');
     videoPlayer.play();
  }
  toggleVideo3(event: any) {
    this.video3On = true;
    let  videoPlayer = this.videoplayer3.nativeElement;
     this.renderer.setStyle(videoPlayer, 'opacity', '1' );
     this.renderer.setStyle(videoPlayer,  'transition', '0.3s  ease-in-out 0.3s ');
     videoPlayer.play();

  }
  toggleVideoEditor(event: any) {
    let  videoPlayer = this.videoPlayerEditor.nativeElement;
     this.renderer.setStyle(videoPlayer, 'opacity', '1' );
     this.renderer.setStyle(videoPlayer, 'z-index', '9998' );
     this.renderer.setStyle(videoPlayer,  'transition', '0.3s  ease-in-out 0.3s ');
     videoPlayer.play();
  }
  hideVideo(event:any){
    let  videoPlayer = this.videoplayer.nativeElement;
    let videoPlayer2 = this.videoplayer2.nativeElement;
    let videoPlayer3 = this.videoplayer3.nativeElement;
    let videoPlayerEditor = this.videoPlayerEditor.nativeElement;
    this.renderer.setStyle(videoPlayer, 'opacity', '0' );
    this.renderer.setStyle(videoPlayer2, 'opacity', '0');
    this.renderer.setStyle(videoPlayer3, 'opacity', '0');
    this.renderer.setStyle(videoPlayerEditor, 'opacity', '0');
    this.video1On = false;
    this.video2On = false;
    this.video3On = false;
  }

}
