import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../services/navbar.service';
import { FooterService } from '../services/footer.service';

@Component({
  selector: 'app-user-terms',
  templateUrl: './user-terms.component.html',
  styleUrls: ['./user-terms.component.scss']
})
export class UserTermsComponent implements OnInit {

  constructor(private nav: NavbarService,
    private footer:FooterService
    ) { }

  ngOnInit(): void {
    this.nav.show();
    this.footer.show();
  }

}
