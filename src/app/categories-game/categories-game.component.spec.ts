import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriesGameComponent } from './categories-game.component';

describe('CategoriesGameComponent', () => {
  let component: CategoriesGameComponent;
  let fixture: ComponentFixture<CategoriesGameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoriesGameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriesGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
