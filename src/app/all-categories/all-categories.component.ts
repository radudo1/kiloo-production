import { Component, OnInit, HostListener } from '@angular/core';
import { FooterService } from '../services/footer.service';
import { NavbarService } from '../services/navbar.service';

@Component({
  selector: 'app-all-categories',
  templateUrl: './all-categories.component.html',
  styleUrls: ['./all-categories.component.scss']
})
export class AllCategoriesComponent implements OnInit {

  screenWidth: number;

  rowsNumber: number;
  columnNumber: number;

  promoFields = [
    {   "Id": 1,
        "Name":"Fighting"
    },
    { "Id": 2,
      "Name":"Girl"
    },
    { "Id": 3,
      "Name":"BasketBall"
    },
    { "Id": 4,
    "Name":"Football"
    },
    { "Id": 5,
    "Name":"Puzzle"
    }
    ,
    { "Id": 6,
    "Name":"Crazy"
    }
    ,
    { "Id": 7,
    "Name":"Fighting"
    }
    ,
    { "Id": 8,
    "Name":"Racing"
    }
    ,
    { "Id": 9,
    "Name":"Idle"
    }
    ,
    { "Id": 10,
    "Name":"war"
    }
    ,
    { "Id": 11,
    "Name":"racing"
    }

    ];

  constructor( private footer: FooterService,
               private nav: NavbarService
     ) { }

  ngOnInit(): void {
    this.getScreenSize();
    this.footer.show();
    this.nav.show();
    window.scrollTo(0,0);
  }

  getDetails(number){
    var gameName = this.promoFields.find(x=>x.Id == number);
    return gameName;
  }
  numSequence(n: number): Array<number> { 
    
    return Array(n); 
    
  } 
  randomInt(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
        this.screenWidth = window.innerWidth; 
        let screen = this.screenWidth;

        if (screen > 1400) 
        {
          this.rowsNumber = 3;
          this.columnNumber = 10;
        }  else if (screen < 1401 && screen > 1160)
        {
          this.rowsNumber = 4;
          this.columnNumber = 8;
        } else if (screen < 1161 && screen > 920)
        {
          this.rowsNumber = 5;
          this.columnNumber = 6;
        } 
        else if (screen < 920)
        {
          this.rowsNumber = 6;
          this.columnNumber = 4;
        }
  }
}
