import { Component, OnInit, Renderer2, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { CookieService } from "../../../node_modules/ngx-cookie-service";
@Component({
  selector: 'app-cookie-consent',
  templateUrl: './cookie-consent.component.html',
  styleUrls: ['./cookie-consent.component.scss']
})
export class CookieConsentComponent implements OnInit {
  modalRef: BsModalRef;
  config = {
    animated: true,
    keyboard: true,
    backdrop: true,
    ignoreBackdropClick: false,
    class: 'modal-lg modal-dialog-centered',
  };
  constructor(private modalService: BsModalService,
    private cookieService: CookieService,
    private renderer : Renderer2) { }

  ngOnInit(): void {
  }
 
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template)
  }
  closeModal(){
      if (!this.modalRef) {
        return;
      }
   
      this.modalRef.hide();
      this.modalRef = null;
  }
  ac(){
    if(this.modalRef){
      this.closeModal();
    }
    var elem = document.getElementById("cookieconsent");
    this.renderer.setStyle(elem,  'transition', '0.3s  ease-in-out 0.3s ');
    this.renderer.setStyle(elem, 'opacity', '0');
    this.cookieService.set('accepted', 'true');  
    console.log(this.cookieService.get('accepted'));  
  }
}
