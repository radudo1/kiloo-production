import { Component, OnInit, HostListener, Inject, Renderer2, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { NavbarService } from '../services/navbar.service';
import { FooterService } from '../services/footer.service';
import { Router, ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { DOCUMENT } from '@angular/common';
import { Position } from "../models/position";
import { Game } from "../models/game";
import { NavGameService } from '../services/nav-game.service';

@Component({
  selector: 'app-game-page',
  templateUrl: './game-page.component.html',
  styleUrls: ['./game-page.component.scss']
})

export class GamePageComponent implements OnInit, OnDestroy {
 ///----------------------------------///
 id: string;
 screenWidth: number;
 isFullScreen: boolean;
 elem: any;
 bottom: any;
 rowsNumber: number;
 columnNumber: number;
 gridPos;
 liked: boolean;
 disliked: boolean;
 
 playAgainC;
 playAgainR;
 clicked: boolean = false;
 adWatched: boolean = false;
 play: boolean = false;
 playClicked: boolean = false;
 playButton;
 fullScreenButton;
 game : Game  = {
   name: "Bullet League Robogedon Test Game Name : Lol",
   developer: "Funday Factory",
   likes: 156,
   dislikes: 89,
   pictureLink: "../../assets/img/playing_game_thumb.png",
   iframeLink: "https://www.kiloo.com/storage/bullet-league-robogeddon/html5",
 };
 gameLink;

 constructor(
   private nav: NavbarService,
   private navGame: NavGameService,
   @Inject(DOCUMENT) private document: any,
   private footer: FooterService,
   public router: Router,
   private activatedRoute: ActivatedRoute,
   private renderer: Renderer2
 ) {}
 
 
 ngOnInit(): void {
   this.nav.hide();
   this.navGame.show();
   this.footer.show();
   this.elem = document.getElementById("iframe-wrapper");
   this.bottom = document.getElementById("iframe--bottom");
   this.playButton = document.getElementById("play-button");

   this.id = this.activatedRoute.snapshot.paramMap.get("id");
   this.getScreenSize();
   this.chkScreenMode();
   //this.countDown();
   this.start();
   
 }

 ngOnDestroy(): void {
   //Called once, before the instance is destroyed.
   //Add 'implements OnDestroy' to the class.
   this.navGame.hide();
   this.clearTimer();
 }

 @HostListener("document:fullscreenchange", ["$event"])
 @HostListener("document:webkitfullscreenchange", ["$event"])
 @HostListener("document:mozfullscreenchange", ["$event"])
 @HostListener("document:MSFullscreenChange", ["$event"])
 fullscreenmodes(event) {
   this.chkScreenMode();
 }
 playClick() {
   if(this.adWatched = true){
     this.clicked = true;
     this.game.iframeLink = this.gameLink;
   }
 }
 chkScreenMode() {
   if (document.fullscreenElement) {
     //fullscreen
     this.isFullScreen = true;
     const iframe: HTMLElement = document.getElementById("iFrame");
     this.renderer.setStyle(iframe, "height", "calc(100% - 90px)");
   } else {
     //not in full screen
     this.isFullScreen = false;
     const iframe: HTMLElement = document.getElementById("iFrame");
     this.renderer.setStyle(iframe, "height", "calc(100%)");
   }
 }

 //Counter 
 intervalId = 0;
 message = '';
 seconds = 6;

 clearTimer() { clearInterval(this.intervalId); }
 start() { this.cDown(); }
 stop()  {
   this.clearTimer();
   this.message = `Holding at T-${this.seconds} seconds`;
 }
  cDown() {
   this.gameLink = this.game.iframeLink;

   this.game.iframeLink = '';
   this.renderer.setStyle(this.playButton, "filter", "grayscale(100%)");
   this.clearTimer();
   this.intervalId = window.setInterval(() => {
     this.seconds -= 1;
     if (this.seconds === 0) {
       this.adWatched = true;
       this.renderer.removeStyle(this.playButton, "filter");
       this.message = 'Blast off!';
       this.stop();
     } else {
       var suffix = " seconds...";
       if(this.seconds === 1){ suffix = "second...";}
       this.message = `You can continue in ${this.seconds} ${suffix} `;
     }
   }, 1000);
 }

 @HostListener("window:resize", ["$event"])
 getScreenSize(event?) {
   this.screenWidth = window.innerWidth;
   let screen = this.screenWidth;
   this.gridPos = new Position();

   if (screen > 1500) {
     this.rowsNumber = 7;
     this.columnNumber = 11;
     this.gridPos.x = "auto / span 6";
     this.gridPos.y = "auto / span 6";
   } else if (screen < 1501 && screen > 1400) {
     this.rowsNumber = 7;
     this.columnNumber = 10;
     this.gridPos.x = "3 / span 6";
     this.gridPos.y = "1 / span 6";
   } else if (screen < 1401 && screen > 1280) {
     this.rowsNumber = 9;
     this.columnNumber = 9;
     this.gridPos.x = "2 / span 6";
     this.gridPos.y = "1 / span 6";
   } else if (screen < 1281 && screen > 1160) {
     this.rowsNumber = 10;
     this.columnNumber = 8;
     this.gridPos.x = "2 / span 6";
     this.gridPos.y = "1 / span 6";
   } else if (screen < 1161 && screen > 1040) {
     this.rowsNumber = 11;
     this.columnNumber = 7;
     this.elem.focus();
     this.gridPos.x = "1 / span 6";
     this.gridPos.y = "1 / span 6";
   } else if (screen < 1401) {
     this.rowsNumber = 13;
     this.columnNumber = 6;
     this.elem.focus();
     this.gridPos.x = "1 / span 6";
     this.gridPos.y = "1 / span 6";
   }
 }

 onLike(game) {
   if (!this.liked && !this.disliked) {
     this.liked = true;
     this.game.likes++;
   } else if (this.liked && !this.disliked) {
     this.liked = false;
     this.game.likes = this.game.likes - 1;
   }
 }

 onDislike(game) {
   if (!this.liked && !this.disliked) {
     this.disliked = true;
     this.game.dislikes++;
   } else if (!this.liked && this.disliked) {
     this.disliked = false;
     this.game.dislikes = this.game.dislikes - 1;
   }
 }

 numSequence(n: number): Array<number> {
   return Array(n);
 }
 openFullscreen() {
   if (this.elem.requestFullscreen) {
     this.elem.requestFullscreen();
   } else if (this.elem.mozRequestFullScreen) {
     /* Firefox */
     this.elem.mozRequestFullScreen();
   } else if (this.elem.webkitRequestFullscreen) {
     /* Chrome, Safari and Opera */
     this.elem.webkitRequestFullscreen();
   } else if (this.elem.msRequestFullscreen) {
     /* IE/Edge */
     this.elem.msRequestFullscreen();
   }
 }
 /* Close fullscreen */
 closeFullscreen() {
   if (this.document.exitFullscreen) {
     this.document.exitFullscreen();
   } else if (this.document.mozCancelFullScreen) {
     /* Firefox */
     this.document.mozCancelFullScreen();
   } else if (this.document.webkitExitFullscreen) {
     /* Chrome, Safari and Opera */
     this.document.webkitExitFullscreen();
   } else if (this.document.msExitFullscreen) {
     /* IE/Edge */
     this.document.msExitFullscreen();
   }
 }
  

}
