import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from "@angular/flex-layout";
import { AngularFireModule} from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from '../environments/environment';
import { AuthService } from './services/auth.service';
import { FormsModule } from '@angular/forms';
import { AngularFirestore } from 'angularfire2/firestore';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgAdblockDetectModule } from 'ng-adblock-detect';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { FooterComponent } from './footer/footer.component';
import { TextComponent } from './text/text.component';
import { MainComponent } from './main/main.component';
import { LoginComponent } from './login/login.component';
import { HotGames11xComponent } from './hot-games/hot-games11x/hot-games11x.component';
import { HotGames10xComponent } from './hot-games/hot-games10x/hot-games10x.component';
import { HotGames9xComponent } from './hot-games/hot-games9x/hot-games9x.component';
import { HotGames8xComponent } from './hot-games/hot-games8x/hot-games8x.component';
import { HotGames7xComponent } from './hot-games/hot-games7x/hot-games7x.component';
import { HotGames6xComponent } from './hot-games/hot-games6x/hot-games6x.component';
import { HotGames5xComponent } from './hot-games/hot-games5x/hot-games5x.component';
import { AuthGuard } from './services/auth-guard.service';
import { Top5xComponent } from './top/top-games2x3/top5x/top5x.component';
import { Main2x3Component } from './layouts/main2x3/main2x3.component';
import { Top6xComponent } from './top/top-games2x3/top6x/top6x.component';
import { Top7xComponent } from './top/top-games2x3/top7x/top7x.component';
import { Top8xComponent } from './top/top-games2x3/top8x/top8x.component';
import { Top9xComponent } from './top/top-games2x3/top9x/top9x.component';
import { Top10xComponent } from './top/top-games2x3/top10x/top10x.component';
import { Top11xComponent } from './top/top-games2x3/top11x/top11x.component';
import { T11xComponent } from './top/top-games3x3/t11x/t11x.component';
import { Main3x3Component } from './layouts/main3x3/main3x3.component';
import { T10xComponent } from './top/top-games3x3/t10x/t10x.component';
import { T9xComponent } from './top/top-games3x3/t9x/t9x.component';
import { T8xComponent } from './top/top-games3x3/t8x/t8x.component';
import { T7xComponent } from './top/top-games3x3/t7x/t7x.component';
import { T6xComponent } from './top/top-games3x3/t6x/t6x.component';
import { T5xComponent } from './top/top-games3x3/t5x/t5x.component';
import { PlayNowComponent } from './play-now/play-now.component';
import { CategoriesComponent } from './categories/categories.component';
import { DisabledGamesComponent } from './disabled-games/disabled-games.component';
import { GamePageComponent } from './game-page/game-page.component';
import { RouterModule } from '@angular/router';
import { HotTestComponent } from './hot-test/hot-test.component';
import { AllGamesComponent } from './all-games/all-games.component';
import { AllCategoriesComponent } from './all-categories/all-categories.component';
import { CommonModule } from '@angular/common';
import { SafePipe } from './safe.pipe';
import { TextGameComponent } from './text-game/text-game.component';
import { CategoriesGameComponent } from './categories-game/categories-game.component';
import { NavGameComponent } from './nav-game/nav-game.component';
import { HotGamePageComponent } from './hot-game-page/hot-game-page.component';
import { PlayAgainComponent } from './play-again/play-again.component';
import { PlayAgainHomeComponent } from './play-again-home/play-again-home.component';
import { HeroJobAdComponent }   from './ads/hero-job-ad/hero-job-ad.component';
import { AdBannerComponent }    from './ads/ad-banner/ad-banner.component';
import { HeroProfileComponent } from './ads/hero-profile/hero-profile.component';
import { AdDirective }          from './services/ad.directive';
import { AdService }            from './services/ad.service';
import { AdComponent } from './ads/ad/ad.component';
import { FullScreenAdComponent } from './ads/full-screen-ad/full-screen-ad.component';
import { FullAdComponent } from './ads/full-ad/full-ad.component';
import { SingelCategoryComponent } from './singel-category/singel-category.component';
import { SubwaySurfersComponent } from './subway-surfers/subway-surfers.component';
import { RatingComponent } from './rating/rating.component';
import {NgcCookieConsentModule} from 'ngx-cookieconsent';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { UserTermsComponent } from './user-terms/user-terms.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { CookieConsentComponent } from './cookie-consent/cookie-consent.component';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AdDetectedComponent } from './ad-detected/ad-detected.component';
import { RatingGameComponent } from './rating-game/rating-game.component';
import { SearchTextPipe } from './search-text.pipe';
import { SearchResultsComponent } from './search-results/search-results.component';
import { CategoriesSearchComponent } from './categories-search/categories-search.component';
import { DataService } from './services/data.service';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { ClickOutsideDirective } from './click-outside.directive';
import { FrisbeeForeverComponent } from './frisbee-forever/frisbee-forever.component';
import { Top3x3Component } from './top3x3/top3x3.component';
import { Top2x3Component } from './top2x3/top2x3.component';


  
@NgModule({
   declarations: [
      AppComponent,
      NavComponent,
      FooterComponent,
      TextComponent,
      MainComponent,
      LoginComponent,
      HotGames11xComponent,
      HotGames10xComponent,
      HotGames9xComponent,
      HotGames8xComponent,
      HotGames7xComponent,
      HotGames6xComponent,
      HotGames5xComponent,
      Top5xComponent,
      Main2x3Component,
      Top6xComponent,
      Top7xComponent,
      Top8xComponent,
      Top9xComponent,
      Top10xComponent,
      Top11xComponent,
      T11xComponent,
      Main3x3Component,
      T10xComponent,
      T9xComponent,
      T8xComponent,
      T7xComponent,
      T6xComponent,
      T5xComponent,
      PlayNowComponent,
      CategoriesComponent,
      DisabledGamesComponent,
      GamePageComponent,
      HotTestComponent,
      AllGamesComponent,
      AllCategoriesComponent,
      SafePipe,
      TextGameComponent,
      CategoriesGameComponent,
      NavGameComponent,
      HotGamePageComponent,
      PlayAgainComponent,
      PlayAgainHomeComponent,
      AdDirective,
      AdBannerComponent,
      HeroJobAdComponent,
      HeroProfileComponent,
      AdComponent,
      FullScreenAdComponent,
      FullAdComponent,
      SingelCategoryComponent,
      SubwaySurfersComponent,
      RatingComponent,
      PrivacyPolicyComponent,
      UserTermsComponent,
      AboutUsComponent,
      CookieConsentComponent,
      AdDetectedComponent,
      RatingGameComponent,
      SearchTextPipe,
      SearchResultsComponent,
      CategoriesSearchComponent,
      SearchBarComponent,
      ClickOutsideDirective,
      FrisbeeForeverComponent,
      Top3x3Component,
      Top2x3Component,
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      CommonModule,
      FlexLayoutModule,
      AngularFireModule.initializeApp(environment.firebase, 'angular-auth-firebase'), 
      NgcCookieConsentModule,
      AngularFireAuthModule,
      AngularFireDatabaseModule,
      RouterModule,
      FormsModule,
      ModalModule.forRoot(),
      NgAdblockDetectModule,
      BrowserAnimationsModule
    
   ],
   providers: [AuthService, AuthGuard, AngularFirestore,AdService, DataService],
   entryComponents: [ HeroJobAdComponent, HeroProfileComponent ],
   bootstrap: [
      AppComponent
   ]
})

export class AppModule { }
//platformBrowserDynamic().bootstrapModule(AppModule);