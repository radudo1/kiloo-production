import { Top2x3Component } from './top2x3/top2x3.component';
import { Top3x3Component } from './top3x3/top3x3.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './services/auth-guard.service';
import { GamePageComponent } from './game-page/game-page.component';
import { AllCategoriesComponent } from './all-categories/all-categories.component';
import { SingelCategoryComponent } from './singel-category/singel-category.component';
import { AllGamesComponent } from './all-games/all-games.component';
import { UserTermsComponent } from './user-terms/user-terms.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { AdDetectedComponent } from './ad-detected/ad-detected.component';
import { SearchResultsComponent } from './search-results/search-results.component';
import { FrisbeeForeverComponent } from './frisbee-forever/frisbee-forever.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full'},
  { path: 'login', component: LoginComponent},
  { path: 'main', component: MainComponent, canActivate: [AuthGuard] },
  { path: 'categories', component:AllCategoriesComponent, canActivate: [AuthGuard]},
  { path: 'game/:id', component: GamePageComponent, canActivate: [AuthGuard] },
  { path: 'category/:name', component: SingelCategoryComponent, canActivate: [AuthGuard] },
  { path: 'games', component: AllGamesComponent, canActivate: [AuthGuard] },
  { path: 'frisbee-forever', component: FrisbeeForeverComponent, canActivate: [AuthGuard] },
  { path: 'privacy-policy', component: UserTermsComponent, canActivate: [AuthGuard] },
  { path: 'about', component: AboutUsComponent, canActivate: [AuthGuard] },
  { path: 'user-terms', component: PrivacyPolicyComponent, canActivate: [AuthGuard] },
  { path: 'ad', component: AdDetectedComponent, canActivate: [AuthGuard] },
  { path: 'search', component: SearchResultsComponent, canActivate: [AuthGuard] },
  { path: 'test', component: Top2x3Component, canActivate: [AuthGuard] },

 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {  }
