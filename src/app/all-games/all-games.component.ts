import { Component, OnInit, HostListener, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { NavbarService } from '../services/navbar.service';
import { FooterService } from '../services/footer.service';
import { Position } from '../models/position';

@Component({
  selector: 'app-all-games',
  templateUrl: './all-games.component.html',
  styleUrls: ['./all-games.component.scss']
})
export class AllGamesComponent implements OnInit {
  screenWidth: number;
  rowsNumber:number;
  threeByThree;
  columnNumber: number;
  video1On: boolean = false;
  video2On: boolean = false;
  video3On: boolean = false;


  constructor(private nav: NavbarService,
    private footer: FooterService,
    private renderer: Renderer2
    ) { }

  ngOnInit(): void {
    this.getScreenSize();
    this.nav.show();
    this.footer.show();
    window.scrollTo(0,0);
  }

  @ViewChild('videoPlayer') videoplayer: ElementRef;
  @ViewChild('videoPlayer2') videoplayer2: ElementRef;
  @ViewChild('videoPlayer3') videoplayer3: ElementRef;
  @ViewChild('videoPlayerEditor') videoPlayerEditor: ElementRef;
  
  toggleVideo(event: any) {
    this.video1On = true;
    let  videoPlayer = this.videoplayer.nativeElement;
    this.renderer.setStyle(videoPlayer, 'opacity', '1' );
    this.renderer.setStyle(videoPlayer,  'transition', '0.3s  ease-in-out 0.3s ');
    videoPlayer.play();

  }
  toggleVideoEditor(event: any) {
    let  videoPlayer = this.videoPlayerEditor.nativeElement;
     this.renderer.setStyle(videoPlayer, 'opacity', '1' );
     this.renderer.setStyle(videoPlayer, 'z-index', '9998' );
     this.renderer.setStyle(videoPlayer,  'transition', '0.3s  ease-in-out 0.3s ');
     videoPlayer.play();
  }
  hideVideo(event:any){
    let  videoPlayer = this.videoplayer.nativeElement;
    let  videoPlayerEditor = this.videoPlayerEditor.nativeElement;
    this.renderer.setStyle(videoPlayer, 'opacity', '0' );
    this.renderer.setStyle(videoPlayerEditor, 'opacity', '0');
    this.video1On = false;
    this.video2On = false;
    this.video3On = false;
  }

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
        this.screenWidth = window.innerWidth; 
        let screen = this.screenWidth;
        this.threeByThree = new Position();
        if ( screen > 1500) 
        {
          this.rowsNumber = 6;
          this.columnNumber = 11;
          this.threeByThree.x =  "auto / span 3";
          this.threeByThree.y =  "auto / span 3";
        } 
        else if (screen < 1501 && screen > 1400)
        {
          this.rowsNumber = 7;
          this.columnNumber = 10;
          this.threeByThree.x =  "8 / span 3";
          this.threeByThree.y =  "1 / span 3";
        }
        else if (screen < 1401 && screen > 1280)
        {
          this.rowsNumber = 8;
          this.columnNumber = 9;
          this.threeByThree.x =  "7 / span 3";
          this.threeByThree.y =  "1 / span 3";
        }
        else if (screen < 1281 && screen > 1160)
        {
          this.rowsNumber = 9;
          this.columnNumber = 8;
          this.threeByThree.x =  "6 / span 3";
          this.threeByThree.y =  "1 / span 3";
        }
        else if (screen < 1161 && screen > 1040)
        {
          this.rowsNumber = 10;
          this.columnNumber = 7;
          this.threeByThree.x =  "5 / span 3";
          this.threeByThree.y =  "1 / span 3";
        }
        else if (screen < 1401 && screen > 920)
        {
          this.rowsNumber = 11;
          this.columnNumber = 6;
          this.threeByThree.x =  "4 / span 3";
          this.threeByThree.y =  "1 / span 3";
        }
        else if (screen < 920)
        {
          this.rowsNumber = 14;
          this.columnNumber = 5;
          this.threeByThree.x =  "3 / span 3";
          this.threeByThree.y =  "1 / span 3";
        }
  }
  
  numSequence(n: number): Array<number> { 
    
    return Array(n); 
    
  } 
}
