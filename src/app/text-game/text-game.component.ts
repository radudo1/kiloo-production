import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-text-game',
  templateUrl: './text-game.component.html',
  styleUrls: ['./text-game.component.scss']
})
export class TextGameComponent implements OnInit {
  @Input('gn') name: string;
  constructor() { }

  ngOnInit(): void {
  }

}
