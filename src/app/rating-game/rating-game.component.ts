import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-rating-game',
  templateUrl: './rating-game.component.html',
  styleUrls: ['./rating-game.component.scss']
})
export class RatingGameComponent implements OnInit {


  @Input() gameName: string;

  constructor() { }

  ngOnInit(): void {
  }

}
