import { TestBed } from '@angular/core/testing';

import { NavGameService } from './nav-game.service';

describe('NavGameService', () => {
  let service: NavGameService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NavGameService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
