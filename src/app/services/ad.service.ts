import { Injectable }           from '@angular/core';
import { HeroJobAdComponent }   from '../ads/hero-job-ad/hero-job-ad.component';
import { HeroProfileComponent } from '../ads/hero-profile/hero-profile.component';
import { AdItem }               from '../ads/ad-item';
@Injectable()
export class AdService {
  getAds() {
    return [
      new AdItem(HeroProfileComponent, {name: 'Ad number 1', bio: 'This is the first Ad'}),
      new AdItem(HeroProfileComponent, {name: 'Ad number 2', bio: 'C00 l ad for cool people'}),
      new AdItem(HeroProfileComponent, {name: 'Ad number 3', bio: 'Smart as they come'}),
      new AdItem(HeroProfileComponent, {name: 'Ad number 5', bio: 'Wow Ads Really work'}),
    ];
  }
}