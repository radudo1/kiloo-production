import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotGamePageComponent } from './hot-game-page.component';

describe('HotGamePageComponent', () => {
  let component: HotGamePageComponent;
  let fixture: ComponentFixture<HotGamePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotGamePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotGamePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
