import { Component, OnInit, HostListener } from '@angular/core';
import { Position } from "../models/position";

@Component({
  selector: 'app-hot-game-page',
  templateUrl: './hot-game-page.component.html',
  styleUrls: ['./hot-game-page.component.scss']
})
export class HotGamePageComponent implements OnInit {
  //--------------------//
  screenWidth:number;
  rowsNumber:number;
  columnNumber:number;
  gridPos;
  //-------------------//

  constructor() { }

  ngOnInit(): void {
    this.getScreenSize();
  }
  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
      this.screenWidth = window.innerWidth; 
      let screen = this.screenWidth;
      this.gridPos = new Position();

      if ( screen > 1500) 
      {
        this.rowsNumber = 2;
        this.columnNumber = 11;
        this.gridPos.x =  "auto / span 2";
        this.gridPos.y =  "auto / span 2";
      } 
      else if (screen < 1501 && screen > 1400)
      {
        this.rowsNumber = 3;
        this.columnNumber = 10;
        this.gridPos.x =  "9 / span 2";
        this.gridPos.y =  "1 / span 2";

      }
      else if (screen < 1401 && screen > 1280)
      {
        this.rowsNumber = 3;
        this.columnNumber = 9;
        this.gridPos.x =  "8 / span 2";
        this.gridPos.y =  "1 / span 2";

      }
      else if (screen < 1281 && screen > 1160)
      {
        this.rowsNumber = 4;
        this.columnNumber = 8;
        this.gridPos.x =  "7 / span 2";
        this.gridPos.y =  "1 / span 2";
      }
      else if (screen < 1161 && screen > 1040)
      {
        this.rowsNumber = 4;
        this.columnNumber = 7;
        this.gridPos.x =  "6 / span 2";
        this.gridPos.y =  "1 / span 2";
      }
      else if (screen < 1401)
      {
        this.rowsNumber = 4;
        this.columnNumber = 6;
        this.gridPos.x =  "5 / span 2";
        this.gridPos.y =  "1 / span 2";
      }
 
    }
}
