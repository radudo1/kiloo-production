import { Component, OnInit, ElementRef, ViewChild, Renderer2, HostListener } from '@angular/core';
import { Language } from '../models/language';
import {NavbarService} from '../services/navbar.service';
import {Animations} from '../animations/slide-in-out/slide-in-out.component';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  // ...
} from '@angular/animations';
import { Router, NavigationStart } from '@angular/router';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
  animations: Animations
})
export class NavComponent implements OnInit {

  screenWidth:number;
  barWidth:number;

  constructor(public nav: NavbarService,

    ) { 
    }


  ngOnInit(): void {
    this.getScreenSize();
  } 
 

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?)  {
        this.screenWidth = window.innerWidth; 
        let screen = this.screenWidth;
        if ( screen > 1500) 
        {
          this.barWidth = 1020;
        } 
        else if (screen < 1501 && screen > 1400)
        {
          this.barWidth =  900;
        }
        else if (screen < 1401 && screen > 1280)
        {
          this.barWidth = 800;
        }
        else if (screen < 1281 && screen > 1160)
        {
          this.barWidth = 710;
        }
        else if (screen < 1161 && screen > 1040)
        {
          this.barWidth = 610;
        }
        else if (screen < 1401 && screen > 920)
        {
          this.barWidth = 490;
        }
        else if (screen < 920)
        {
          this.barWidth = 380;
        }

  }
    

}
