import { Component, OnInit } from '@angular/core';
import {RatingService} from '../services/rating.service';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss']
})
export class RatingComponent implements OnInit {

  constructor(public rating: RatingService) { }
  
  ngOnInit(): void {
  }

}
