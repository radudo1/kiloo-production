import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayAgainHomeComponent } from './play-again-home.component';

describe('PlayAgainHomeComponent', () => {
  let component: PlayAgainHomeComponent;
  let fixture: ComponentFixture<PlayAgainHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayAgainHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayAgainHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
