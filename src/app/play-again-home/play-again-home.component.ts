import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-play-again-home',
  templateUrl: './play-again-home.component.html',
  styleUrls: ['./play-again-home.component.scss']
})
export class PlayAgainHomeComponent implements OnInit {

 ///----------------------------------///
 screenWidth: number;

 rowsNumber:number;
 columnNumber:number;
///---------------------------------///

   constructor(


   ) {}

   ngOnInit(): void {

     this.getScreenSize();
   }

   @HostListener('document:fullscreenchange', ['$event'])
   @HostListener('document:webkitfullscreenchange', ['$event'])
   @HostListener('document:mozfullscreenchange', ['$event'])
   @HostListener('document:MSFullscreenChange', ['$event'])



   @HostListener('window:resize', ['$event'])
   getScreenSize(event?) {
       this.screenWidth = window.innerWidth; 
       let screen = this.screenWidth;


       if ( screen > 1500) 
       {
         this.rowsNumber = 7;
         this.columnNumber = 11;

       } 
       else if (screen < 1501 && screen > 1400)
       {
         this.rowsNumber = 7;
         this.columnNumber = 10;

       }
       else if (screen < 1401 && screen > 1280)
       {
         this.rowsNumber = 8;
         this.columnNumber = 9;


       }
       else if (screen < 1281 && screen > 1160)
       {
         this.rowsNumber = 9;
         this.columnNumber = 8;

       }
       else if (screen < 1161 && screen > 1040)
       {
         this.rowsNumber = 10;
         this.columnNumber = 7;

       }
       else if (screen < 1401 && screen > 920)
       {
         this.rowsNumber = 11;
         this.columnNumber = 6;

       }
       else if (screen < 920)
       {
         this.rowsNumber = 14;
         this.columnNumber = 5;

       }
 }

     
   numSequence(n: number): Array<number> { 
     return Array(n); 
   } 
}
