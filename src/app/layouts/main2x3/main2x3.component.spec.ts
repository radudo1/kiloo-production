import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Main2x3Component } from './main2x3.component';

describe('Main2x3Component', () => {
  let component: Main2x3Component;
  let fixture: ComponentFixture<Main2x3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Main2x3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Main2x3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
