import { Component, OnInit, HostListener } from '@angular/core';
import { NavbarService } from '../../services/navbar.service';
import { FooterService } from '../../services/footer.service';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-main2x3',
  templateUrl: './main2x3.component.html',
  styleUrls: ['./main2x3.component.scss']
})
export class Main2x3Component implements OnInit {

  screenWidth: number;


  constructor(
    private nav: NavbarService,
    private footer: FooterService,
    private router: Router,
    public authService: AuthService,
    ) {
      this.getScreenSize();
  }
  ngOnInit(){
    this.nav.show();
    this.footer.show();

  }

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
        this.screenWidth = window.innerWidth;
    

  }
}
