import { Component, OnInit, HostListener} from '@angular/core';
import { NavbarService } from "../../services/navbar.service";
import { FooterService } from "../../services/footer.service";
import { AuthService } from "../../services/auth.service";
import { T11xComponent } from "../../top/top-games3x3/t11x/t11x.component";
import { Router } from "@angular/router";
@Component({
  selector: 'app-main3x3',
  templateUrl: './main3x3.component.html',
  styleUrls: ['./main3x3.component.scss']
})
export class Main3x3Component implements OnInit {
  screenWidth: number;
  content: any;

  constructor(
    private nav: NavbarService,
    private footer: FooterService,
    private router: Router,
    public authService: AuthService,
    ) {
      this.getScreenSize();
  }
  ngOnInit(){
    this.nav.show();
    this.footer.show();

  }
  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
        this.screenWidth = window.innerWidth; 
        let screen = this.screenWidth;

        if ( screen > 1500) 
        {
          this.content = T11xComponent;
        } 
        else if (screen < 1501 && screen > 1400)
        {
          this.content = 't10x';
        }
        else if (screen < 1401 && screen > 1280)
        {
          this.content = '<app-t10x></app-t9x>';
        }
        else if (screen < 1281 && screen > 1160)
        {
          this.content = '<app-t10x></app-t8x>';
        }
        else if (screen < 1161 && screen > 1040)
        {
          this.content = '<app-t10x></app-t7x>';
        }
        else if (screen < 1401 && screen > 920)
        {
          this.content = '<app-t10x></app-t6x>';
        }
        else if (screen < 920)
        {
          this.content = '<app-t10x></app-t5x>';
        }
  }
}
