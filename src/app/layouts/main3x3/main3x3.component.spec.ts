import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Main3x3Component } from './main3x3.component';

describe('Main3x3Component', () => {
  let component: Main3x3Component;
  let fixture: ComponentFixture<Main3x3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Main3x3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Main3x3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
